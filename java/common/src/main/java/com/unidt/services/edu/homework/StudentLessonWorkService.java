package com.unidt.services.edu.homework;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.ChapterWorkInfo;
import com.unidt.mybatis.bean.LessonChapterInfo;
import com.unidt.mybatis.bean.LessonInfo;
import com.unidt.mybatis.bean.StudentWorkInfo;
import com.unidt.mybatis.dto.ContentDto;
import com.unidt.mybatis.dto.LessonChapterIDDto;
import com.unidt.mybatis.dto.LessonIDDto;
import com.unidt.mybatis.dto.StudentWorkDto;
import com.unidt.mybatis.mapper.ChapterWorkInfoMapper;
import com.unidt.mybatis.mapper.LessonInfoMapper;
import com.unidt.mybatis.mapper.StudentWorkMapper;
import com.unidt.mybatis.mapper.WorkInfoMapper;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 根据课程ID获取作业
 */
@Service
public class StudentLessonWorkService {

    @Autowired
    StudentWorkMapper   workMapper;
    @Autowired
    LessonInfoMapper lessonInfoMapper;
    @Autowired
    ChapterWorkInfoMapper chapterWorkInfoMapper;

    /**
     *  根据课程获取学生作业完成情况
     * @param lesson_id
     * @return
     */
    public String getStudentWorkByLesson(String lesson_id) {

        List<StudentWorkDto> infos = workMapper.getAllStudentWorkByLesson(lesson_id);
        List<Document> docs = new ArrayList<>();

        for (StudentWorkDto info: infos) {
            Document doc = new Document("lesson_id",info.lesson_id).append("work_id", info.work_id)
                    .append("score", info.score).append("student_id", info.student_id)
                    .append("student_no", info.student_no).append("student_name", info.student_name);
            docs.add(doc);
        }

        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", docs).toJson();
    }


    /**
     *  根据课程和作业id获取学生作业提交率
     *    根据lesson_id 获取学生总数
     *    （用于计算平均得分和提交率）
     * @param lesson_chapter_id
     * @param work_id
     * @return
     */
    public String getLessonWorkByworkId(String lesson_chapter_id,String work_id) {

        LessonInfo lessonInfo = lessonInfoMapper.selectLessonByID(lesson_chapter_id);
        if(lessonInfo == null){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "课程为空").toJson();
        }
        int studentNum = lessonInfo.student_num;

        //根据作业和课程id获取学生作业情况
        List<StudentWorkDto> infos = workMapper.getStudentWorkById(lesson_chapter_id,work_id);
        StudentWorkInfo studentWorkInfo = new StudentWorkInfo();
        studentWorkInfo.lesson_chapter_id = lesson_chapter_id;
        studentWorkInfo.work_id = work_id;
        StudentWorkDto dto = workMapper.getSumAndCountById(studentWorkInfo);
        String  avg = "0";
        String rate = "0";
        DecimalFormat df = new DecimalFormat("0.00");//格式化小数
        int score=dto.sum_score;
        int count = dto.all_count;
        if (dto.all_count !=0){
            avg = df.format((float)score/count);//返回的是String类型
        }
        if(studentNum!=0){
            rate = df.format((float)count/studentNum);
        }
        if(infos.size() == 0){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "查询为空").toJson();
        }
        //获取总分数
        List<Document> docs = new ArrayList<>();

        for (StudentWorkDto info: infos) {
            Document doc = new Document("lesson_chapter_id",info.lesson_chapter_id).append("work_id", info.work_id)
                    .append("score", info.score).append("student_id", info.student_id)
                    .append("student_no", info.student_no).append("student_name", info.student_name);
            docs.add(doc);
        }
        Document alldoc = new Document("lesson_chapter_id",lesson_chapter_id).append("work_id", work_id)
                .append("lesson_sum", studentNum).append("submit_sum", dto.all_count).append("avg_score",avg).
                        append("submit_rate",rate).append("worklist",docs);

        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", alldoc).toJson();
    }

    /**
     *   获取当前作业提交率
     * @param lessonIDDto
     * @return
     */
    public String getStudentWorkPercentByLesson(LessonIDDto lessonIDDto) {
        String lesson_id = lessonIDDto.lesson_id;
        List<String> chapterWorkIDList = chapterWorkInfoMapper.selectChapterWorkByLessonID(lesson_id);
        LessonInfo object = lessonInfoMapper.selectLessonByID(lesson_id);
        ArrayList<Document> docA = new ArrayList<>();
        for (String chapterWorkID: chapterWorkIDList){
            Document doc = new Document();
            doc.append("work_id", chapterWorkID);
            LessonChapterIDDto lessonChapterIDDto = new LessonChapterIDDto();
            lessonChapterIDDto.lesson_id = lesson_id;
            lessonChapterIDDto.work_id = chapterWorkID;
            List<StudentWorkDto> infos = workMapper.getAllStudentWorkByLessonWork(lessonChapterIDDto);
            ChapterWorkInfo CWInfo = chapterWorkInfoMapper.selectChapterWorkByID(chapterWorkID);
            try {
                doc.append("question_content", CWInfo.question_content)
                        .append("question_type", CWInfo.question_type);
            }catch (Exception e){
                return ReturnResult.createResult(Constants.API_CODE_NOT_FOUND, "不存在的work_id").toJson();
            }
            double percent = 0;
            int done = infos.size();
            if(object.student_num ==null){
                object.student_num = 0;
            }
            if (object.student_num == 0) {
                percent = 0;
            } else {
                percent = done / (double)object.student_num;
            }
            doc.append("Percent", percent);
            docA.add(doc);
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", docA).toJson();
    }

    /**
     *  根据课程ID学生ID获取当前课程作业提交情况
     * @param lessonIDDto
     *
     * @return
     */
    public String getStudentWorkByLessonStudent(LessonIDDto lessonIDDto) {
        String lesson_id = lessonIDDto.lesson_id;
        String student_id = lessonIDDto.student_id;
        //获取章节id
        List<LessonChapterInfo> chapterList  = chapterWorkInfoMapper.selectChapterLessonId(lesson_id);
        if (StringUtils.isBlank(lesson_id) ||StringUtils.isBlank(student_id)){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "参数不正确").toJson();
        }
        ArrayList<Document> docChapters = new ArrayList<>();
        for(LessonChapterInfo info :chapterList){
            List<ChapterWorkInfo> chapterWorkList = chapterWorkInfoMapper.selectWorkByChapterId(info.chapter_id);
            //Document doc = new Document();
            ArrayList<Document> docWorks = new ArrayList<>();
            LessonChapterIDDto lessonChapterIDDto = new LessonChapterIDDto();
            lessonChapterIDDto.lesson_chapter_id = info.lesson_chapter_id;
            for(ChapterWorkInfo workInfo:chapterWorkList){
                Document work = new Document();
                lessonChapterIDDto.work_id = workInfo.id;
                lessonChapterIDDto.student_id = student_id;
                List<StudentWorkDto> stuInfos  =  workMapper.getLessonoChapyerByLessonWork(lessonChapterIDDto);
                work.append("status", 0);
                for (StudentWorkDto stu: stuInfos){
                    work.append("score", stu.score).append("status", 1).append("lesson_chapter_id",stu.lesson_chapter_id).append("student_lesson_id",stu.student_work_id);
                }
                work.append("question_content", workInfo.question_content).append("question_type", workInfo.question_type)
                        .append("work_id",workInfo.id).append("lesson_chapter_id",info.lesson_chapter_id);
                docWorks.add(work);
                docChapters.add(work);
            }
            //doc.append("lesson_chapter_id",info.lesson_chapter_id).append("workList", docWorks);
            //docChapters.add(doc);
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", docChapters).toJson();
    }
}



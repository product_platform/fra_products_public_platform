package com.unidt.services.edu.Student;

import com.unidt.api.LoginController;
import com.unidt.helper.common.Constants;
import com.unidt.mybatis.dto.StudentInfoDto;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.StudentInfo;
import com.unidt.mybatis.dto.TeacherClassSearchDto;
import com.unidt.mybatis.mapper.StudentInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.bson.Document;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class StudentInfomation {
    private static Logger log = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    StudentInfoMapper studentInfoMapper;

    /**
     * 根据学生ID 查询学生详情
     * @param studentInfoDto
     * @return
     */
    public String selectStudentInfo(StudentInfoDto studentInfoDto){
        StudentInfo object = studentInfoMapper.selectStudentByID(studentInfoDto.student_id);
        if (object==null) {
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "查询为空").toJson();
        }
        Document doc = new Document("id", studentInfoDto.student_id).append("student_no", object.student_no)
                .append("student_name", object.student_name).append("student_phone", object.student_phone)
                .append("student_mail",object.student_mail).append("student_sex", object.studen_sex)
                .append("student_age", object.studen_age).append("class_id",object.class_id);
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").append("data", doc).toJson();
    }

    /**
     * 根据教师ID查询教师班级下的学生详情
     * @param teacherClassSearchDto
     * @return
     */
    public String teacherClassSearchStudent(TeacherClassSearchDto teacherClassSearchDto){
        List<Document> temp = new ArrayList<>();
        List<StudentInfo> SInfoList = studentInfoMapper.selectStudentByClassTeacher(teacherClassSearchDto.teacher_id);
        if (SInfoList==null) {
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "查询为空").toJson();
        }
        for (StudentInfo aSInfoList : SInfoList) {
            if (Arrays.asList(teacherClassSearchDto.class_id).contains(aSInfoList.class_id)) {
                temp.add(new Document("student_id", aSInfoList.student_id).append("student_no", aSInfoList.student_no)
                        .append("student_name", aSInfoList.student_name).append("class_id", aSInfoList.class_id)
                        .append("student_sex", aSInfoList.studen_sex).append("student_age", aSInfoList.studen_age)
                        .append("student_mail", aSInfoList.student_mail).append("student_phone", aSInfoList.student_phone));
            }
        }
        return ReturnResult.createResult(200,"ok").append("data", temp).toJson();
    }

}

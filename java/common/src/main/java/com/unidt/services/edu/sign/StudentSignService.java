package com.unidt.services.edu.sign;


import com.unidt.helper.common.Constants;
import com.unidt.mybatis.dto.StudentSignDto;
import com.unidt.helper.common.GetUUID32;
import com.unidt.helper.common.GetformatData;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.StudentLessonInfo;
import com.unidt.mybatis.mapper.StudentLessonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentSignService {
    @Autowired
    StudentLessonMapper studentLessonMapper;

    /**
     * 根据学生ID 课程ID 的学生签到
     * @param studentSignDto
     * @return
     */
    public String studentSign(StudentSignDto studentSignDto){
        StudentLessonInfo SLInfo = new StudentLessonInfo();
        SLInfo.student_lesson_id = GetUUID32.getUUID32();
        SLInfo.student_id = studentSignDto.student_id;
        SLInfo.lesson_id = studentSignDto.lesson_id;
        SLInfo.signin_time = GetformatData.getformatData();
        SLInfo.signout_time = null;
        SLInfo.del_flag = "0";
        studentLessonMapper.insertStudentLesson(SLInfo);
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").toJson();
    }
}

package com.unidt.services.edu.courselesson;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.GetUUID32;
import com.unidt.helper.common.GetformatData;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.*;
import com.unidt.mybatis.dto.CopyLessonChapterDto;
import com.unidt.mybatis.dto.LessonDetail;
import com.unidt.mybatis.dto.LessonInfoDto;
import com.unidt.mybatis.mapper.*;
import com.unidt.services.edu.tools.SSOService;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class LessonChapterService {
    @Autowired
    LessonChapterInfoMapper chapterInfoMapper;
    @Autowired
    ChapterResourcesInfoMapper chapterResourcesInfoMapper;
    @Autowired
    TeacherResourcesMapper teacherResourcesMapper;
    @Autowired
    LessonInfoMapper lessonInfoMapper;
    @Autowired
    LessonChapterInfoMapper lessonChapterInfoMapper;
    @Autowired
    CourseLessonService courseLessonService;
    @Autowired
    SSOService SSOService;

    /**
     * 根据lesson_id获取课程下章节资源详情以及教师资源详情
     * @param lessonID
     * @return
     */
    public String getLessonChapterDetail(String lessonID) {
        List<LessonDetail> lessonDetails = chapterInfoMapper.getLessonDetail(lessonID);
        Document ret = ReturnResult.createResult(Constants.API_CODE_OK,"OK");
        List<Document> docs = new ArrayList<>();
        List<Document> doc_detail = new ArrayList<>();

        for (LessonDetail detail: lessonDetails) {
            ArrayList<Document> crl = new ArrayList<>();
            ArrayList<Document> trl = new ArrayList<>();
            List<ChapterResourcesInfo> object_cList = chapterResourcesInfoMapper.selectChapterResourcesByChapterID(detail.chapter_id);
            for (ChapterResourcesInfo object_c : object_cList){
                crl.add(new Document("chapter_id", object_c.chapter_id).append("resource_id", object_c.resource_id).append("resource_name", object_c.resource_name)
                .append("resource_size", object_c.resource_size).append("resource_type", object_c.resource_type).append("resource_url", object_c.resource_url)
                .append("resource_cover", object_c.resource_cover));
            }
            List<TeacherResourcesInfo> object_tList = teacherResourcesMapper.selectTeacherResourcesByChapterID(detail.chapter_id);
            for (TeacherResourcesInfo object_t : object_tList){
                trl.add(new Document("chapter_id", object_t.chapter_id).append("teacher_resource_id", object_t.teacher_resource_id).append("resource_name", object_t.resource_name)
                        .append("resource_size", object_t.resource_size).append("resource_type", object_t.resource_type).append("last_play_time", object_t.last_play_time)
                        .append("resource_url", object_t.resource_url).append("resource_cover", object_t.resource_cover));
            }
            Document doc = new Document("lesson_id", detail.lesson_id)
                    .append("chapter_id", detail.chapter_id).append("chapter_name", detail.chapter_name)
                    .append("chapter_title", detail.chapter_title).append("resource_detail", doc_detail)
                    .append("chapter_resource_list", crl).append("teacher_resource_list", trl);
            docs.add(doc);
        }
        return ret.append("data", docs).toJson();
    }

    /**
     * 根据teacher_id，class_id，course_id，lesson_date，class_start_time查询备课详情，没有则保存
     * @param copyLessonChapterDto
     * @return
     */
    public String copyLessonChapter(CopyLessonChapterDto copyLessonChapterDto) {
        LessonInfo LInfo = lessonInfoMapper.selectLessonInfoByMulti(copyLessonChapterDto);
        if (LInfo == null) {
            LInfo = new LessonInfo();
            LInfo.lesson_id = GetUUID32.getUUID32();
            LInfo.create_time = GetformatData.getformatData();
            Map checkToken = SSOService.getUserByToken(copyLessonChapterDto.token);
            System.out.println(checkToken);
            LInfo.create_user = checkToken.get("user").toString();
            LInfo.del_flag = Constants.DEL_FLAG_0;
            LInfo.schedule_status = Constants.LESSON_CHAPTER_01;
            LInfo.lesson_right = Constants.LESSON_RIGHT_1;
            LInfo.start_date = copyLessonChapterDto.start_date;
            LInfo.end_date = copyLessonChapterDto.end_date;
            LInfo.class_id = copyLessonChapterDto.class_id;
            LInfo.course_id = copyLessonChapterDto.course_id;
            LInfo.teacher_id = copyLessonChapterDto.teacher_id;
            LInfo.lesson_date = copyLessonChapterDto.lesson_date;
            LInfo.class_start_time = copyLessonChapterDto.class_start_time;
            lessonInfoMapper.insertLesson(LInfo);
        }
        for (String chapter_id : copyLessonChapterDto.chapterList) {
            LessonChapterInfo LCInfo = new LessonChapterInfo();
            LCInfo.lesson_chapter_id = GetUUID32.getUUID32();
            LCInfo.lesson_id = LInfo.lesson_id;
            LCInfo.chapter_id = chapter_id;
            LCInfo.del_flag = "0";
            LCInfo.status = "01";
            lessonChapterInfoMapper.insertGrade(LCInfo);
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "ok").toJson();
    }

    /**
     * 根据班级ID查询已排课的科目名与ID
     * @param class_id
     * @return
     */
    public String lessonGetByClassID(String class_id) {
        if(StringUtils.isEmpty(class_id)){
           return  ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "参数错误").toJson();
        }
        List<LessonInfoDto> LInfoList = lessonInfoMapper.selectCourseByClassID(class_id);
        ArrayList<String> ListAll = new ArrayList<>();
        ArrayList<Document> docAll = new ArrayList<>();
        for (LessonInfoDto LInfo: LInfoList){
            if (!ListAll.contains(LInfo.course_id)){
                Document doc = new Document();
                ListAll.add(LInfo.course_id);
                doc.append("course_name", LInfo.course_name).append("course_id", LInfo.course_id);
                docAll.add(doc);
            }
        }
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").append("data", docAll).toJson();
    }


    /**
     *
     */
}

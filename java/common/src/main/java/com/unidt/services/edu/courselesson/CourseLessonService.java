package com.unidt.services.edu.courselesson;


import ch.qos.logback.core.rolling.helper.IntegerTokenConverter;
import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.*;
import com.unidt.mybatis.dto.*;
import com.unidt.mybatis.mapper.*;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.unidt.helper.common.Constants.API_CODE_NOT_FOUND;
import static com.unidt.helper.common.Constants.API_CODE_OK;

@Service
public class CourseLessonService {
    @Autowired
    private    TeacherInfoMapper teacherInfoMapper;
    @Autowired
    private   ClassInfoMapper  classInfoMapper;
    @Autowired
    private   LessonInfoMapper lessonInfoMapper;

    @Autowired
    private   CourseInfoMapper courseInfoMapper;
    @Autowired
    private LessonChapterInfoMapper lessonChapterInfoMapper;

    @Autowired
    private TeacherResourcesMapper teacherResourcesMapper;
    @Autowired
    private SSOMapper ssoMapper;

    @Autowired
    private StudentLessonMapper studentLessonMapper;
    @Autowired
    private StudentInfoMapper studentInfoMapper;
    /**
     *  根据教师id获取全部日程
     *
     * @param id
     * @return
     */
    public String  getTeacherLessonById(String id,String status){

        String res = null;
        if(StringUtils.isEmpty(id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "教师编号不可为空").toJson();
        }
        System.out.println("ID=" + id);
        TeacherInfo info = teacherInfoMapper.selectTeacherByID(id);
        if(info == null) {
             return   ReturnResult.createResult(API_CODE_NOT_FOUND, "当前教师不存在").toJson();
        }
        try {
            ArrayList<LessonInfoDto>   lessonInfoList = lessonInfoMapper.getCourseLessonList(id);
            if(StringUtils.isEmpty(status)){
                lessonInfoList = lessonInfoMapper.getCourseLessonList(id);
            }else{
                String teacher_id = id;
                lessonInfoList= lessonInfoMapper.getCourseLessonListBystatus(teacher_id,status);
            }
            if(lessonInfoList.size()==0){
                 return   ReturnResult.createResult(API_CODE_OK, "查询成功！").toJson();
            }
            List<Document> docs = new ArrayList<>();
            for (LessonInfoDto infoDto: lessonInfoList) {
                docs.add(
                        new Document("teacher_id", infoDto.teacher_id).append("course_id", infoDto.course_id)
                                .append("course_name", infoDto.course_name).append("class_id", infoDto.class_id)
                                .append("play_chapter_node", infoDto.play_chapter_node).append("play_resource_node", infoDto.play_resource_node)
                                .append("course_type", infoDto.course_type).append("covermap_url", infoDto.covermap_url)
                                .append("class_name", infoDto.class_name).append("schedule_status", infoDto.schedule_status)
                                .append("lesson_right", infoDto.lesson_right).append("class_start_time", infoDto.class_start_time)
                                .append("lesson_date",infoDto.lesson_date).append("lesson_id",infoDto.lesson_id)
                );
            }
            res =   ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data", docs).toJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return   res;
    }

    /**
     *   老师根据时间端，班级，科目，查询
     *      （班级，科目。状态均单选，为空时查询全部）
     * @param courseLessonDto
     * @return
     */
    public String  getTeacherLessonByInfo(CourseLessonDto courseLessonDto){

        String res = null;
        if(courseLessonDto ==null || StringUtils.isEmpty(courseLessonDto.id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "教师编号不可为空").toJson();
        }
        if(StringUtils.isEmpty(courseLessonDto.class_id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "班级不可为空").toJson();
        }
        if( StringUtils.isEmpty(courseLessonDto.course_id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "科目不可为空").toJson();
        }
        if( StringUtils.isEmpty(courseLessonDto.start_date)||StringUtils.isEmpty(courseLessonDto.end_date)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "查询期间不可为空").toJson();
        }
        String id = courseLessonDto.id;
        TeacherInfo info = teacherInfoMapper.selectTeacherByID(id);
        if(info == null) {
            return   ReturnResult.createResult(API_CODE_NOT_FOUND, "当前教师不存在").toJson();
        }
        try {
            ArrayList<LessonInfoDto>   lessonInfoList = lessonInfoMapper.selectTeacherLessonByInfo(courseLessonDto);
            if(lessonInfoList.size()==0){
                return   ReturnResult.createResult(API_CODE_OK, "查询成功！").toJson();
            }
            List<Document> docs = new ArrayList<>();
            for (LessonInfoDto infoDto: lessonInfoList) {
                docs.add(
                        new Document("teacher_id", infoDto.teacher_id).append("course_id", infoDto.course_id)
                                .append("course_name", infoDto.course_name).append("class_id", infoDto.class_id)
                                .append("play_chapter_node", infoDto.play_chapter_node).append("play_resource_node", infoDto.play_resource_node)
                                .append("course_type", infoDto.course_type).append("covermap_url", infoDto.covermap_url)
                                .append("class_name", infoDto.class_name).append("schedule_status", infoDto.schedule_status)
                                .append("lesson_right", infoDto.lesson_right).append("class_start_time", infoDto.class_start_time)
                                .append("lesson_date",infoDto.lesson_date).append("lesson_id",infoDto.lesson_id)

                );
            }
            res =   ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data", docs).toJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return   res;
    }

    /**
     *   根据班级，科目，时间段获取日程信息
     * @param courseLessonDto
     * @return
     */
    public String  getLessonByInfo(CourseLessonDto courseLessonDto){

        String res = null;
        if(courseLessonDto ==null || StringUtils.isEmpty(courseLessonDto.id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "教师编号不可为空").toJson();
        }
        if(StringUtils.isEmpty(courseLessonDto.class_id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "班级不可为空").toJson();
        }
        if( StringUtils.isEmpty(courseLessonDto.course_id)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "科目不可为空").toJson();
        }
        if( StringUtils.isEmpty(courseLessonDto.start_date)||StringUtils.isEmpty(courseLessonDto.end_date)){
            return    ReturnResult.createResult(API_CODE_NOT_FOUND, "查询期间不可为空").toJson();
        }
        try {
            ArrayList<LessonInfoDto>  lessonInfoList = null;
            String id = courseLessonDto.id;
            if(StringUtils.isEmpty(id)){
                String class_id = courseLessonDto.class_id;
                ClassInfo info = classInfoMapper.selectClassByID(class_id);
                if(info == null) {
                    return   ReturnResult.createResult(API_CODE_NOT_FOUND, "当前班级不存在").toJson();
                }
                 lessonInfoList = lessonInfoMapper.selectStudentLessonByInfo(courseLessonDto);
            }else{
                TeacherInfo info = teacherInfoMapper.selectTeacherByID(id);
                if(info == null) {
                    return   ReturnResult.createResult(API_CODE_NOT_FOUND, "当前教师不存在").toJson();
                }
                  lessonInfoList = lessonInfoMapper.selectTeacherLessonByInfo(courseLessonDto);
            }

            if(lessonInfoList.size()==0){
                return   ReturnResult.createResult(API_CODE_OK, "查询成功！").toJson();
            }
            List<Document> docs = new ArrayList<>();
            for (LessonInfoDto infoDto: lessonInfoList) {
                docs.add(
                        new Document("teacher_id", infoDto.teacher_id).append("course_id", infoDto.course_id)
                                .append("course_name", infoDto.course_name).append("class_id", infoDto.class_id)
                                .append("play_chapter_node", infoDto.play_chapter_node).append("play_resource_node", infoDto.play_resource_node)
                                .append("course_type", infoDto.course_type).append("covermap_url", infoDto.covermap_url)
                                .append("class_name", infoDto.class_name).append("schedule_status", infoDto.schedule_status)
                                .append("lesson_right", infoDto.lesson_right).append("class_start_time", infoDto.class_start_time)
                                .append("lesson_date",infoDto.lesson_date).append("lesson_id",infoDto.lesson_id)
                );
            }
            res =   ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data", docs).toJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return   res;
    }
    /**
     * 根据条件获取当前教师课程
     *  教师，科目。班级。
     *
     */
    public  String  getTeacherLesson(CourseLessonDto courseLessonDto){
        String res = null;
        ArrayList<LessonInfoDto> lessonInfoDtolist = null;
        if(courseLessonDto == null || StringUtils.isEmpty(courseLessonDto.id)){
            return   ReturnResult.createResult(API_CODE_NOT_FOUND, "当前教师不存在").toJson();
        }

        //判断班级是否为空，可多选，中间以逗号分开
      if(StringUtils.isEmpty(courseLessonDto.class_id)){
            courseLessonDto.class_id=null;
        }

        if(StringUtils.isEmpty(courseLessonDto.course_id)){
            courseLessonDto.course_id=null;
        }

        //状态不可为空,并格式化，为空时查询全部状态
        if(StringUtils.isEmpty(courseLessonDto.schedule_status)){
            courseLessonDto.schedule_status = null;
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //判断开始时间是否为空
            if(StringUtils.isEmpty(courseLessonDto.start_date)){
                courseLessonDto.start_date=null;
            }

            //判断结束时间是否为空
            if(StringUtils.isEmpty(courseLessonDto.end_date)){
                courseLessonDto.end_date=null;
            }

            lessonInfoDtolist =  lessonInfoMapper.getTeacherLessonList(courseLessonDto);

            List<Document> docs = new ArrayList<>();
            for (LessonInfoDto infoDto: lessonInfoDtolist) {
                int is_ready = 0;
                if(!StringUtils.isEmpty(infoDto.lesson_id)){
                    is_ready = lessonInfoMapper.getLessonIsReady(infoDto.lesson_id);
                }
                if(is_ready>0){
                    is_ready=1;
                }
                docs.add(
                        new Document("teacher_id", infoDto.teacher_id).append("course_id", infoDto.course_id)
                                .append("course_name", infoDto.course_name).append("class_id", infoDto.class_id)
                                .append("play_chapter_node", infoDto.play_chapter_node).append("play_resource_node", infoDto.play_resource_node)
                                .append("course_type", infoDto.course_type).append("covermap_url", infoDto.covermap_url)
                                .append("class_name", infoDto.class_name).append("schedule_status", infoDto.schedule_status)
                                .append("lesson_right", infoDto.lesson_right).append("class_start_time", infoDto.class_start_time)
                                .append("lesson_date",infoDto.lesson_date).append("lesson_id",infoDto.lesson_id).append("is_ready",is_ready)
                );
            }
            res =  ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data",docs).toJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 根据条件获取当前学生
     *  教师，科目。班级。
     */
    public  String  getStudentLesson(CourseLessonDto courseLessonDto){
        String res = null;
        ArrayList<LessonInfoDto> lessonInfoDtolist = null;
        if(courseLessonDto == null || StringUtils.isEmpty(courseLessonDto.class_id)){
            return   ReturnResult.createResult(API_CODE_NOT_FOUND, "班级不可为空").toJson();
        }
        //科目不可为空,可多选，中间以逗号分开
        if(StringUtils.isEmpty(courseLessonDto.course_id)){
            courseLessonDto.course_id =null;
        }
        //状态不可为空,并格式化，为空时查询全部状态
        if(StringUtils.isEmpty(courseLessonDto.schedule_status)){
            courseLessonDto.schedule_status = null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        try {
            //判断开始时间是否为空
            if(StringUtils.isEmpty(courseLessonDto.start_date)){
                courseLessonDto.start_date=null;
            }
            //判断结束时间是否为空
            if(StringUtils.isEmpty(courseLessonDto.end_date)){
                courseLessonDto.end_date=null;
            }
            lessonInfoDtolist =  lessonInfoMapper.getStudentLessonList(courseLessonDto);

            List<Document> docs = new ArrayList<>();
            for (LessonInfoDto infoDto: lessonInfoDtolist) {
                docs.add(
                        new Document("teacher_id", infoDto.teacher_id).append("course_id", infoDto.course_id)
                                .append("course_name", infoDto.course_name).append("class_id", infoDto.class_id)
                                .append("play_chapter_node", infoDto.play_chapter_node).append("play_resource_node", infoDto.play_resource_node)
                                .append("course_type", infoDto.course_type).append("covermap_url", infoDto.covermap_url)
                                .append("class_name", infoDto.class_name).append("schedule_status", infoDto.schedule_status)
                                .append("lesson_right", infoDto.lesson_right).append("class_start_time", infoDto.class_start_time)
                                .append("lesson_date",infoDto.lesson_date).append("lesson_id",infoDto.lesson_id)
                );
            }
            res =  ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data",docs).toJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     *   新增修改备课
     * @param lessonChapterDto
     * @return
     */
     public String setLessonChapter(LessonChapterDto lessonChapterDto){
         String res = null;
         if(lessonChapterDto ==null){
             res =  ReturnResult.createResult(API_CODE_NOT_FOUND, "参数错误！").toJson();
             return res;
         }
         if(StringUtils.isEmpty(lessonChapterDto.lesson_id)){
             res =  ReturnResult.createResult(API_CODE_NOT_FOUND, "参数错误！").toJson();
             return res;
         }
         List<String> chapterList = lessonChapterDto.chapterList;
         String lesson_id = lessonChapterDto.lesson_id;
         if(chapterList.size()>0){
             //删除旧备课内容
            // lessonChapterInfoMapper.deleteByLesson(lesson_id);
             lessonChapterInfoMapper.deleteByList(chapterList,lesson_id);
             //已有的
             List<LessonChapterInfo> chapterInfos = lessonChapterInfoMapper.getChapterListByLessonId(lesson_id);
             for(String chapter_id : chapterList){
                 String  oldChapter_id = "";
                 for(LessonChapterInfo in : chapterInfos){
                     if ((chapter_id).equals(in.chapter_id)){
                         oldChapter_id =in.chapter_id;
                         continue;
                     }
                 }
                 if("".equals(oldChapter_id)){
                     UUID uuid = UUID.randomUUID();
                     LessonChapterInfo info = new LessonChapterInfo();
                     info.chapter_id = chapter_id;
                     info.lesson_id = lesson_id;
                     info.del_flag = Constants.DEL_FLAG_0;
                     info.chapter_parent_id ="";
                     String  uuids = uuid.toString().replace("-","");
                     info.lesson_chapter_id =  uuids;
                     info.status = Constants.LESSON_CHAPTER_01;
                     // 01未开始，02进行中，03，已完成，04已取消
                     lessonChapterInfoMapper.insertGrade(info);
                 }
             }
             res =  ReturnResult.createResult(API_CODE_OK, "备课保存成功！").toJson();
         }
             return res;
     }

    /**
     *    根据课程获取备课信息
     * @param lessonChapterDto
     * @return
     */
     public String getLessonChapter(LessonChapterDto lessonChapterDto){
         String res = null;
         if(lessonChapterDto ==null){
             res =  ReturnResult.createResult(API_CODE_NOT_FOUND, "参数错误！").toJson();
             return res;
         }
         if(StringUtils.isEmpty(lessonChapterDto.lesson_id)){
             res =  ReturnResult.createResult(API_CODE_NOT_FOUND, "参数错误！").toJson();
             return res;
         }
         String lesson_id = lessonChapterDto.lesson_id;
         List<String>  chapterList  = lessonChapterInfoMapper.getLessonChapterById(lesson_id);
         ArrayList<Document> docAll = new ArrayList<>();
         for (String chapter: chapterList){
             Document docChapter = new Document();
             List<TeacherResourcesInfo> trInfoList = teacherResourcesMapper.selectTeacherResourcesByChapterID(chapter);

             ArrayList<Document> resourceList = new ArrayList<Document>();
             for (TeacherResourcesInfo trInfo: trInfoList){
                 Document docResource = new Document("resource_name", trInfo.resource_name).append("teacher_resource_id", trInfo.teacher_resource_id)
                         .append("resource_type", trInfo.resource_type).append("resource_url", trInfo.resource_url)
                         .append("resource_size", trInfo.resource_size);
                 resourceList.add(docResource);
             }
             docChapter.append("chapter_id", chapter).append("resource_list", resourceList);
             docAll.add(docChapter);
         }
         Document doc = new Document();
         if(chapterList.size() !=0 ){
             doc =  new Document("lesson_id", lesson_id)
                     .append("chapterList", docAll);

         }
         res =  ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data",doc).toJson();
         return res;
     }

    /**
     * 查询科目详情
     * @param id
     * @return
     */
    public String getCourseInfoById(String id){
        String res = null;
        if(StringUtils.isEmpty(id)){
            res =  ReturnResult.createResult(API_CODE_NOT_FOUND, "参数错误！").toJson();
        }
        CourseInfo  info = new CourseInfo();
        info = courseInfoMapper.selectCourseByID(id);
        Document doc = new Document();
        if(info !=null){
             doc =  new Document("course_id", info.course_id)
                     .append("course_no",info.course_no).append("course_name",info.course_name)
                     .append("course_desc",info.course_desc).append("course_grade",info.course_grade)
                     .append("covermap_url",info.course_desc).append("course_type",info.course_grade)
                     .append("del_flag",info.del_flag).append("course_level_id",info.course_level_id).append("create_time",info.create_time);
        }

        res =  ReturnResult.createResult(API_CODE_OK, "查询成功！").append("data",doc).toJson();
        return res;
    }


    /**
     * 将数组转换为指定分隔符连接的字符串
     *
     * @param sourArray
     *            待转换数组
     * @param splitter
     *            分隔符
     * @param invertedComma
     *            引号类型
     * @return String
     */
    public String formatStr(String[] sourArray, String splitter, String invertedComma) {
        String midSpl = invertedComma + splitter + invertedComma;
        String result = invertedComma + org.apache.commons.lang3.StringUtils.join(sourArray, midSpl) + invertedComma;
        return result;
    }

    /**
     *  新增单节排课
     * @param lessonInfoDto
     * @return
     */
    public String getLessonInfoDetail(LessonInfoDto lessonInfoDto){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String res = null;

        if(StringUtils.isEmpty(lessonInfoDto.lesson_date)){
            return  ReturnResult.createResult(API_CODE_NOT_FOUND, "参数错误！").toJson();
        }
        LessonInfo lessonInfo = new LessonInfo();
        CopyLessonChapterDto copyDto = new CopyLessonChapterDto();
        copyDto.class_id=lessonInfoDto.class_id;
        copyDto.course_id = lessonInfoDto.course_id;
        copyDto.teacher_id = lessonInfoDto.teacher_id;
        copyDto.class_start_time = lessonInfoDto.class_start_time;
        LessonInfo info = lessonInfoMapper.selectLessonInfoByMulti(copyDto);
        if(info==null){
            UUID uuid = UUID.randomUUID();
            String  uuids = uuid.toString().replace("-","");
            lessonInfo.lesson_id = uuids;
            lessonInfo.create_time = sdf.format(new Date());
            lessonInfo.class_id=lessonInfoDto.class_id;
            lessonInfo.course_id = lessonInfoDto.course_id;
            lessonInfo.teacher_id = lessonInfoDto.teacher_id;
            lessonInfo.class_start_time = lessonInfoDto.class_start_time;
            lessonInfo.lesson_date = lessonInfoDto.lesson_date;
            lessonInfo.create_user = lessonInfoDto.create_user;
            //状态（01未开始，02进行中，03，已完成，04已取消）
            lessonInfo.del_flag = Constants.DEL_FLAG_0;
            lessonInfo.schedule_status = Constants.LESSON_CHAPTER_01;
            lessonInfo.lesson_right =Constants.LESSON_RIGHT_0;
            lessonInfo.start_date= lessonInfoDto.lesson_date;
            lessonInfo.end_date = lessonInfoDto.lesson_date;
            String lesson_id = lessonInfoMapper.selectLessonIDByStartTime(lessonInfo);
            if (lesson_id != null){
                return ReturnResult.createResult(Constants.API_CODE_INNER_ERROR, "该时间段已有排课").toJson();
            }
            lessonInfoMapper.insertLesson(lessonInfo);
        }
        res =  ReturnResult.createResult(API_CODE_OK, "排课保存成功！").toJson();
        return res;
    }



    /**
     * 根据ID删除教师上传资源
     * @param teacherClassCourse
     * @return
     */
    public Object delTeacherResource(TeacherClassCourse teacherClassCourse) {
        List<String> strList = teacherClassCourse.strList;
        if(strList.size()==0){
            return ReturnResult.createResult(Constants.API_CODE_OK, "ok");
        }
        for(String str:strList){
            teacherResourcesMapper.deleteTeacherResourcesByID(str);
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "ok");
    }

    /**
     * 获取当前课程学生签到比率
     * @param lesson_id
     * @return
     */
    public String getCheckLesson(String lesson_id){
        if(StringUtils.isEmpty(lesson_id)){
         return    ReturnResult.createResult(Constants.API_CODE_NOT_FOUND, "参数错误！").toJson();
        }
        //获取当前课程人数
        LessonInfo info = lessonInfoMapper.selectLessonByID(lesson_id);
        String rate = "0";
        if(info == null){
          return  ReturnResult.createResult(Constants.API_CODE_NOT_FOUND, "当前课程不存在！").toJson();
        }
        Document doc = new Document();
        Integer lessonSum = info.student_num;
        Integer checkSum = studentLessonMapper.getSumStudentByLesson(lesson_id);
        if (lessonSum==null || lessonSum==0){
            lessonSum =studentInfoMapper.getStudentCount(info.class_id);
        }
        if(lessonSum !=null && lessonSum !=0 && checkSum != null && checkSum !=0){
            DecimalFormat df = new DecimalFormat("0.00");//格式化小数
            rate = df.format((float)checkSum/lessonSum);
        }
        doc.append("rate",rate).append("lessonCount",lessonSum).append("checkCount",checkSum);
        return ReturnResult.createResult(Constants.API_CODE_OK,"OK").append("data", doc).toJson();
    }

    /**
     * 根据教师id、科目id、班级id 获取班级下每个学生的出勤率和作业提交率
     *  ysc
     * @param courseLessonDto
     * @return
     */
    public String getStudentLessonRate(CourseLessonDto courseLessonDto){
        if(courseLessonDto ==null  || StringUtils.isEmpty(courseLessonDto.teacher_id)
                ||StringUtils.isEmpty(courseLessonDto.class_id)
                ||StringUtils.isEmpty(courseLessonDto.course_id)
                ||StringUtils.isEmpty(courseLessonDto.start_date)
                ||StringUtils.isEmpty(courseLessonDto.end_date)){
            return    ReturnResult.createResult(Constants.API_CODE_NOT_FOUND, "参数错误！").toJson();
        }
        List<Document> docs =  new ArrayList<Document>();
        try{
            int workCount = 0;
            //作业总数
            workCount = lessonInfoMapper.getLessonWorkCount(courseLessonDto);
            int studentWorkCount = 0;
            //学生总数
            List<StudentInfo>  studentInfoList = studentInfoMapper.selectInfoByClass(courseLessonDto.class_id);

            if(studentInfoList.size()==0){
                return   ReturnResult.createResult(Constants.API_CODE_OK, "ok").toJson();
            }
            for(StudentInfo stu : studentInfoList){
                courseLessonDto.student_id = stu.student_id;
                //学生签到率
                Double checkRate = 0.00;
                //作业提交率
                Double workRate = 0.00;
                //学生提交作业数
                studentWorkCount  = lessonInfoMapper.getStudentWorkCount(courseLessonDto);

                Document doc = new Document();
                doc.append("student_id",stu.student_id).append("student_no",stu.student_no).append("student_name",stu.student_name);

                if(studentWorkCount!=0 && workCount!=0){
                    workRate = (double)studentWorkCount/workCount;
                }
                //学生签到总数  学生签到总数
                CourseLessonDto count = studentLessonMapper.getCountLessonStudent(courseLessonDto);
                int checkCount = count.checkCount;
                int lessonCount = count.lessonCount;
                if (checkCount!=0 && lessonCount !=0){
                    checkRate =(double)checkCount/lessonCount;
                }
                doc.append("checkRate",checkRate).append("workRate",workRate);
                docs.add(doc);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ReturnResult.createResult(Constants.API_CODE_OK,"OK").append("data", docs).toJson();
    }

    /**
     *  根据当前课程lesson的科目和班级，获取当前班级的科目备课情况
     * @param courseLessonDto
     * @return
     */
    public String getCourseLesson(CourseLessonDto courseLessonDto){
        if(courseLessonDto==null || StringUtils.isEmpty(courseLessonDto.class_id)||StringUtils.isEmpty(courseLessonDto.course_id)){
            return    ReturnResult.createResult(Constants.API_CODE_NOT_FOUND, "参数错误！").toJson();
        }
        Document resDoc= new Document();
        List<Document> docs =  new ArrayList<Document>();
        List<String>  chapterList  = lessonChapterInfoMapper.getCourseChapterByClass(courseLessonDto);
        resDoc.append("class_id",courseLessonDto.class_id).append("course_id",courseLessonDto.course_id);
        if (chapterList.size()>0){
            for (String chapter: chapterList){
                Document doc = new Document();
                doc.append("chapter_id",chapter);
                docs.add(doc);
            }
        }
        resDoc.append("chapterLists",docs);
        return ReturnResult.createResult(Constants.API_CODE_OK,"OK").append("data", resDoc).toJson();
    }
}

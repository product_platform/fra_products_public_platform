package com.unidt.services.edu.schedual.teacher;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.LessonInfo;
import com.unidt.mybatis.bean.TeacherInfo;
import com.unidt.mybatis.dto.TeacherClassCourse;
import com.unidt.mybatis.mapper.LessonInfoMapper;
import com.unidt.mybatis.mapper.StudentInfoMapper;
import com.unidt.mybatis.mapper.StudentLessonMapper;
import com.unidt.mybatis.mapper.TeacherInfoMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherInfoService {
    @Autowired
    private TeacherInfoMapper infoMapper;

    @Autowired
    private LessonInfoMapper lessonInfoMapper;


    @Autowired
    private StudentLessonMapper studentLessonMapper;


    @Autowired
    private StudentInfoMapper studentInfoMapper;

    /**
     * 根据ID获取教师基本信息
     *
     * @param id
     * @return
     */
    public String getTeacherInfo(String id) {
        TeacherInfo info = infoMapper.selectTeacherByID(id);
        Document doc = new Document("id", id).append("teacher_no", info.teacher_no)
                .append("teacher_name", info.teacher_name).append("teacher_phone", info.teacher_phone)
                .append("teacher_mail", info.teacher_mail);

        return doc.toJson();
    }

    /**
     * 获取老师的科目、班级信息
     *
     * @param id
     * @return
     */
    public String getCourseClassInfo(String id) {
        List<TeacherClassCourse> courses = lessonInfoMapper.getTeacherCourseClass(id);

        List<Document> docs = new ArrayList<>();
        for (TeacherClassCourse classCourse : courses) {
            docs.add(
                    new Document("teacher_id", classCourse.teacher_id).append("course_id", classCourse.course_id)
                            .append("course_name", classCourse.course_name).append("class_id", classCourse.class_id)
                            .append("class_name", classCourse.class_name)
            );
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", docs).toJson();
    }

    /**
     * 根据老师id获取课程信息     * @param id
     *
     * @return
     */
    public String getTeacherClassById(String id) {
        List<TeacherClassCourse> courses = lessonInfoMapper.getTeacherClassInfo(id);

        List<Document> docs = new ArrayList<>();
        for (TeacherClassCourse classCourse : courses) {
            docs.add(
                    new Document("teacher_id", classCourse.teacher_id).append("class_status", classCourse.class_status)
                            .append("class_desc", classCourse.course_name).append("class_id", classCourse.class_id)
                            .append("class_name", classCourse.class_name)
            );
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", docs).toJson();
    }

    /**
     * 根据老师id获取老师名下科目信息
     *
     * @param id
     * @return
     */
    public String getTeacherCourseById(String id) {

        List<TeacherClassCourse> courses = lessonInfoMapper.getTeacherCourseInfo(id);
        List<Document> docs = new ArrayList<>();
        for (TeacherClassCourse classCourse : courses) {
            docs.add(
                    new Document("teacher_id", classCourse.teacher_id).append("course_id", classCourse.course_id)
                            .append("course_name", classCourse.course_name).append("course_grade", classCourse.course_grade)
                            .append("course_desc", classCourse.class_name).append("covermap_url", classCourse.covermap_url)
                            .append("course_type", classCourse.course_type)
            );
        }
        return ReturnResult.createResult(Constants.API_CODE_OK, "OK").append("data", docs).toJson();
    }
}



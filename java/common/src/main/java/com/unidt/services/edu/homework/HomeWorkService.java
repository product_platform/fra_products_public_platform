package com.unidt.services.edu.homework;

import com.unidt.api.LoginController;
import com.unidt.helper.common.Constants;
import com.unidt.mybatis.bean.*;
import com.unidt.mybatis.dto.*;
import com.unidt.helper.common.GetUUID32;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.ChapterWorkInfo;
import com.unidt.mybatis.bean.StudentWorkInfo;
import com.unidt.mybatis.bean.WorkInfo;
import com.unidt.mybatis.mapper.*;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

@Service
public class HomeWorkService {
    private static Logger log = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    SSOMapper ssoMapper;
    @Autowired
    WorkInfoMapper workInfoMapper;
    @Autowired
    ChapterWorkInfoMapper chapterWorkInfoMapper;
    @Autowired
    LessonInfoMapper lessonInfoMapper;
    @Autowired
    ClassInfoMapper classInfoMapper;
    @Autowired
    StudentWorkMapper studentWorkMapper;

    /**
     * 教师发布作业
     * @param publishWorkDto
     * @return
     */
    public String publishWork(PublishWorkDto publishWorkDto){
        WorkInfo SWInfo = new WorkInfo();
        SWInfo.work_id = GetUUID32.getUUID32();
        SWInfo.work_desc = publishWorkDto.work_desc;
        SWInfo.course_id = publishWorkDto.course_id;
        SWInfo.work_url = publishWorkDto.work_url;
        SWInfo.chapter_id = publishWorkDto.chapter_id;
        SWInfo.work_name = publishWorkDto.work_name;

        workInfoMapper.insertWorkInfo(SWInfo);

        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").toJson();
    }

    /**
     * 根据chapter_id查询章节作业详情并返回
     * @param publishWorkDto
     * @return
     */
    public String chapterHomework(PublishWorkDto publishWorkDto){
        System.out.println(publishWorkDto.chapter_id);
        ChapterWorkInfo info= chapterWorkInfoMapper.selectChapterWorkByChapterID(publishWorkDto.chapter_id);
        if (info==null) {
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "查询为空").toJson();
        }
        Document doc = new Document("id", info.id).append("question_content", info.question_content)
                .append("question_type", info.question_type).append("question_score", info.question_score).append("sort", info.sort)
                .append("create_time", info.create_time).append("create_user", info.create_user);

        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").append("data", doc).toJson();
    }

    /**
     * 根据teacher_id, course_id 查询班级和课程详情并返回
     * @param classHomeWorkDto
     * @return
     */
    public String classHomeworkByDate(ClassHomeWorkDto classHomeWorkDto){
        LessonCourseInfoDto info = new LessonCourseInfoDto();
        info.teacher_id = classHomeWorkDto.teacher_id;
        info.course_id = classHomeWorkDto.course_id;
        List<LessonCourseInfoDto> LInfoList = lessonInfoMapper.selectLessonByMulti(info);

        ArrayList<Document> dualist = new ArrayList<>();
        if (LInfoList==null) {
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "查询为空").toJson();
        }
        for (LessonCourseInfoDto LInfo: LInfoList){
            if (Arrays.asList(classHomeWorkDto.class_id).contains(LInfo.class_id)){
                if (classHomeWorkDto.date.matches(LInfo.lesson_date)){
                    ClassInfo object = classInfoMapper.selectClassByID(LInfo.class_id);
                    Document doc = new Document();
                    doc.append("class_id", LInfo.class_id);
                    doc.append("class_name", object.class_name);
                    doc.append("class_start_time", LInfo.class_start_time);
                    doc.append("schedule_status", LInfo.schedule_status);
                    doc.append("lesson_id", LInfo.lesson_id);
                    doc.append("course_name", LInfo.course_name);
                    doc.append("lesson_date", LInfo.lesson_date);
                    doc.append("student_num", LInfo.student_num);
                    dualist.add(doc);
                }
            }
        }
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").append("data", dualist).toJson();
    }

    /**
     * 根据chapter_work_id查询作业题干，关联作业问题表返回问题内容，详情和ID
         student_id 为空时获取题和选项，不为空时获取学生答题内容和分值
     * @param homeWorkContentDto
     * @return
     */
    public String homeworkContent(HomeWorkContentDto homeWorkContentDto){

        if (homeWorkContentDto ==null ||"".equals(homeWorkContentDto.work_id)){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "参数错误").toJson();
        }

        StudentWorkInfo studentWorkInfo = new StudentWorkInfo();
        studentWorkInfo.work_id = homeWorkContentDto.work_id;
        studentWorkInfo.student_id = homeWorkContentDto.student_id;
        List<WorkContentAnswerDto> WCAInfo = new ArrayList<>();
        WCAInfo = chapterWorkInfoMapper.selectStudentWorkContentByWorkID(homeWorkContentDto);
        ArrayList<Document> ListContent = new ArrayList<>();
        if (WCAInfo.size() == 0){
            WCAInfo = chapterWorkInfoMapper.selectChapterWorkContentByWorkID(homeWorkContentDto.work_id);
        }
        int score = 0;
        String student_answer = "";
        Document docAll = new Document("question_content", WCAInfo.get(0).question_content).append("question_type", WCAInfo.get(0).question_type);

        for (WorkContentAnswerDto info: WCAInfo){
            Document doc = new Document("content", info.content).append("is_correct_answer", info.is_correct_answer).append("order_sort", info.order_sort)
                    .append("chapter_work_answer_id", info.id).append("lesson_chapter_id", info.lesson_chapter_id);
            ListContent.add(doc);
            student_answer = info.student_answer;
            score = info.score==null?0:info.score;
        }
        docAll.append("score",score).append("student_answer",student_answer);
        docAll.append("answer_list", ListContent);
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").append("data", docAll).toJson();
    }
}

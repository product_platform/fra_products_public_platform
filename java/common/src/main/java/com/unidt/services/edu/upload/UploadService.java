package com.unidt.services.edu.upload;

import com.unidt.api.LoginController;
import com.unidt.helper.common.Constants;
import com.unidt.mybatis.bean.LessonChapterInfo;
import com.unidt.mybatis.dto.CorrectWorkDto;
import com.unidt.mybatis.dto.UploadDto;
import com.unidt.helper.common.GetUUID32;
import com.unidt.helper.common.GetformatData;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.bean.SSO;
import com.unidt.mybatis.dto.UploadStudentWorkDto;
import com.unidt.mybatis.bean.StudentWorkInfo;
import com.unidt.mybatis.mapper.*;
import com.unidt.services.edu.tools.FileTools;
import com.unidt.mybatis.bean.TeacherResourcesInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UploadService{
    private static Logger log = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    TeacherResourcesMapper teacherResourcesMapper;
    @Autowired
    StudentWorkMapper studentWorkMapper;
    @Autowired
    SSOMapper ssoMapper;
    @Autowired
    LessonChapterInfoMapper lessonChapterInfoMapper;
    @Autowired
    ChapterWorkInfoMapper chapterWorkInfoMapper;

    /**
     * 判断lesson_chapter_id是否存在，不存在新建tab_lesson_chapter关联再保存教师资源表
     * @param uploadDto
     * @return
     */
    public String saveFile(UploadDto uploadDto){
        String lesson_chapter_id = lessonChapterInfoMapper.selectLCIDByLessonIDChapterID(uploadDto);
        if (lesson_chapter_id == null){
            LessonChapterInfo info = new LessonChapterInfo();
            lesson_chapter_id = GetUUID32.getUUID32();
            info.lesson_chapter_id = lesson_chapter_id;
            info.lesson_id = uploadDto.lesson_id;
            info.chapter_id = uploadDto.chapter_id;
            info.status = "01";
            info.del_flag = "0";
            lessonChapterInfoMapper.insertGrade(info);
        }
        TeacherResourcesInfo info = new TeacherResourcesInfo();
        info.lesson_chapter_id = lesson_chapter_id;
        info.teacher_id = uploadDto.teacher_id;
        info.resource_desc = uploadDto.resource_desc;
        info.resource_type = uploadDto.resource_type;
        info.resource_right = uploadDto.resource_right;
        info.last_play_time = uploadDto.last_play_time;
        info.chapter_id = uploadDto.chapter_id;
        info.resource_url = uploadDto.resource_url;
        info.resource_cover = uploadDto.resource_cover;
        info.resource_name = uploadDto.resource_name;
        info.teacher_resource_id = GetUUID32.getUUID32();
        if (uploadDto.resource_url == null){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "URL路径为空").toJson();
        }
        info.resource_size = String.valueOf(FileTools.fileDetails(uploadDto.resource_url));
        info.del_flag = 0;

        SSO object = ssoMapper.getSSOByToken(uploadDto.token);
        if (object == null){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "token验证失败").toJson();
        }
        info.create_user = object.user_id;
        System.out.println("save"+GetformatData.getformatData());
        info.create_time = GetformatData.getformatData();
        System.out.println(info.create_time);
        info.update_time = null;
        info.update_user = null;
        teacherResourcesMapper.insertTeacherResources(info);
        log.debug("保存文件" + info.resource_name + "成功");
        return ReturnResult.createResult(200,"ok").toJson();
    }

    /**
     * 教师更新教师资源
     * @param uploadDto
     * @return
     */
    public String updateFile(UploadDto uploadDto){
        TeacherResourcesInfo info = new TeacherResourcesInfo();
        info.lesson_chapter_id = lessonChapterInfoMapper.selectLCIDByLessonIDChapterID(uploadDto);
        if (info.lesson_chapter_id == null){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "不存在该条数据").toJson();
        }
        info.teacher_id = uploadDto.teacher_id;
        info.resource_desc = uploadDto.resource_desc;
        info.resource_type = uploadDto.resource_type;
        info.resource_right = uploadDto.resource_right;
        info.last_play_time = uploadDto.last_play_time;
        info.chapter_id = uploadDto.chapter_id;
        info.resource_url = uploadDto.resource_url;
        info.resource_cover = uploadDto.resource_cover;
        info.resource_name = uploadDto.resource_name;
        info.resource_size = String.valueOf(FileTools.fileDetails(uploadDto.resource_url));
        info.teacher_resource_id = uploadDto.teacher_resource_id;
        info.resource_size = String.valueOf(FileTools.fileDetails(uploadDto.resource_url));
        SSO object = ssoMapper.getSSOByToken(uploadDto.token);
        if (object == null){
            return ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败").toJson();
        }
        info.update_user = object.user_id;
        System.out.println("update"+GetformatData.getformatData());
        info.update_time = GetformatData.getformatData();

        teacherResourcesMapper.updateTeacherResources(info);
        log.debug("更新文件" + info.resource_name + "成功");
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").toJson();
    }

    /**
     * 学生上传作业，如果作业已存在，根据作业ID更新作业
     * @param uploadStudentWorkDto
     * @return
     */
    public String uploadStudentWork (UploadStudentWorkDto uploadStudentWorkDto){
        StudentWorkInfo USWInfo = new StudentWorkInfo();
        SSO object = ssoMapper.getSSOByToken(uploadStudentWorkDto.token);
        if(StringUtils.isBlank(uploadStudentWorkDto.lesson_chapter_id)||StringUtils.isBlank(uploadStudentWorkDto.work_id)){

            return ReturnResult.createResult(Constants.API_CODE_INNER_ERROR, "参数错误，保存失败！").toJson();
        }
        if(object == null){
            return ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败").toJson();
        }
        USWInfo.student_id = object.user_id;

        USWInfo.student_work_id = GetUUID32.getUUID32();
        USWInfo.lesson_id = uploadStudentWorkDto.lesson_id;
        USWInfo.work_id = uploadStudentWorkDto.work_id;
        USWInfo.lesson_chapter_id = uploadStudentWorkDto.lesson_chapter_id;
        StudentWorkInfo work = studentWorkMapper.selectStudentWorkByMulty(USWInfo);

        USWInfo.update_time = GetformatData.getformatData();

        USWInfo.content = uploadStudentWorkDto.content;
        USWInfo.work_url = uploadStudentWorkDto.work_url;

        USWInfo.student_work_url = uploadStudentWorkDto.student_work_url;
        USWInfo.lesson_chapter_id = uploadStudentWorkDto.lesson_chapter_id;
        System.out.println(USWInfo.lesson_chapter_id);
        USWInfo.work_status = "1";

        CorrectWorkDto correctWork = chapterWorkInfoMapper.selectWorkAnswerByID(USWInfo.work_id); // 学生作业打分
        if (correctWork == null){
            return ReturnResult.createResult(Constants.API_CODE_FORBIDDEN, "题库有误，查询该work_id的正确答案为空").toJson();
        }
        if (correctWork.question_type.equals("1")||correctWork.question_type.equals("2")){
            if (USWInfo.content.equals(correctWork.content)){
                USWInfo.score = correctWork.question_score;
            }
        } else { //TODO else 处理为简答题时情况，完全匹配才判断为正确
            if (USWInfo.content.equals(correctWork.content)){
                USWInfo.score = correctWork.question_score;
            }
        }

        if(work != null){
            USWInfo.student_work_id = uploadStudentWorkDto.student_work_id;
            studentWorkMapper.updateStudentWork(USWInfo);
            log.debug("更新学生作业");
        } else {
            studentWorkMapper.insertStudentWork(USWInfo);
            log.debug("提交学生作业");
        }
        return ReturnResult.createResult(Constants.API_CODE_OK,"ok").toJson();
    }
}

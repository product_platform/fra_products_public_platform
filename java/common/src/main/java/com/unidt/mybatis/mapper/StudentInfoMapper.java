package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.StudentInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentInfoMapper {

    @Select("select * from tab_student_info where student_id = #{id}")
    public StudentInfo selectStudentByID(String id);

    @Select("select * from tab_student_info where class_id in (select class_id from tab_lesson_info where teacher_id = #{teacher_id})")
    public List<StudentInfo> selectStudentByClassTeacher(String teacher_id);

    @Select("select s.student_id,s.student_no,s.student_name from tab_student_info s where s.class_id = #{class_id}")
    public List<StudentInfo> selectInfoByClass(String class_id);

    @Select("select COUNT(s.student_id) from tab_student_info s where s.class_id = #{id} and s.del_flag=0")
    public Integer getStudentCount(String class_id);

    @Insert("insert into tab_student_info values(#{student_id},#{student_no},#{student_name}," +
            "#{school_id},#{grade_id},#{class_id},#{studen_sex}," +
            "#{studen_age},#{student_mail},#{student_phone},#{login_user}),#{login_pwd},#{del_flag}," +
            "#{create_user},#{create_time},#{update_user},#{update_time})")
    public void insertStudent(StudentInfo info);

    @Update("update tab_student_info set student_no = #{student_no}," +
            "student_name=#{student_name}, school_id= #{school_id}, grade_id=#{grade_id}, class_id= #{class_id}," +
            "studen_sex=#{studen_sex}, studen_age= #{studen_age}, student_mail=#{student_mail}, student_phone= #{student_phone}," +
            "login_user=#{login_user}, login_pwd= #{login_pwd}, del_flag=#{del_flag}, create_user= #{create_user}," +
            "create_time= #{create_time}, update_user=#{update_user}, update_time= #{update_time} where student_id = #{student_id}")
    public void updateStudent(StudentInfo info);

    @Delete("delete from tab_student_info where student_id=#{id}")
    public void deleteStudentByID(String id);

}


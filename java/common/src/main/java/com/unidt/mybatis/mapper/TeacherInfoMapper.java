package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.TeacherInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface TeacherInfoMapper {

    @Select("select t.* from tab_teacher_info t where t.teacher_id = #{id}")
    public TeacherInfo selectTeacherByID(String id);

    @Insert("insert into tab_teacher_info values(#{teacher_id},#{teacher_no},#{teacher_pwd}," +
            "#{teacher_name},#{teacher_phone},#{teacher_mail},#{teacher_grade}," +
            "#{school_id},#{teacher_status},#{teacher_level},#{del_flag}),#{create_user},#{create_time}," +
            "#{update_user},#{update_time})")
    public void insertTeacher(TeacherInfo info);

    @Update("update tab_teacher_info set teacher_no = #{teacher_no}," +
            "teacher_pwd=#{teacher_pwd}, teacher_name= #{teacher_name} where teacher_id = #{teacher_id}")
    public void updateTeacher(TeacherInfo info);

    @Delete("delete from tab_teacher_info where teacher_id=#{id}")
    public void deleteTeacherByID(String id);

}


package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SchoolInfo {
    public String school_id;
    public String school_no;
    public String school_name;
    public String school_type;
    public String school_desc;
    public String school_status;
    public String school_address;
    public char del_flag;
    public String school_user;
    public String school_phone;
}

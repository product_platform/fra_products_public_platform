package com.unidt.mybatis.dto;

public class TeacherClassSearchDto {
    public String teacher_id;
    public String[] class_id;
    public String token;
    public String  create_user;
}

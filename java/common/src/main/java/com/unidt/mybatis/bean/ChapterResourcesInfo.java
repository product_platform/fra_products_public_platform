package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChapterResourcesInfo {
    public String resource_id;
    public String resource_name;
    public String resource_desc;
    public String resource_type;
    public String resource_url;
    public String resource_cover;
    public String resource_right;
    public String resource_size;
    public String resource_status;
    public String chapter_id;
    public char del_flag;
    public String create_user;
    public String create_time;
    public String update_user;
    public String update_time;
}

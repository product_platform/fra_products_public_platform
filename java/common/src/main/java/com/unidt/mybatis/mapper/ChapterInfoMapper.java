package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.ChapterInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface ChapterInfoMapper {

    @Select("select * from tab_chapter_info where chapter_id = #{id}")
    public ChapterInfo selectChapterByID(String id);

    @Insert("insert into tab_chapter_info values(#{chapter_id},#{chapter_name},#{chapter_title},#{chapter_content},#{chapter_level},#{course_id}," +
    "#{chapter_parentid}, #{del_flag}, #{create_time},#{update_time})")
    public void insertChapter(ChapterInfo info);

    @Update("update tab_chapter_info set chapter_name = #{chapter_name}, chapter_title = #{chapter_title}," +
            "chapter_content = #{chapter_content}, chapter_level = #{chapter_level}, course_id = #{course_id}," +
            "chapter_parentid = #{chapter_parentid}, del_flag = #{del_flag}, create_time = #{create_time}, update_time = #{update_time} where chapter_id = #{chapter_id}")
    public void updateChapter(ChapterInfo info);

    @Delete("delete from tab_chapter_info where chapter_id=#{id}")
    public void deleteChapter(String id);

}


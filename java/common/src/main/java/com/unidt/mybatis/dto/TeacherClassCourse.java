package com.unidt.mybatis.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TeacherClassCourse {
    public String teacher_id; // 教师id
    public String course_id; // 课程ID
    public String course_name;// 课程名
    public String class_id; //班级ID
    public String class_name; //班级名
    public String course_desc;
    public String course_grade;
    public String course_type;
    public String covermap_url;
    public String grade_id;
    public String grade_name;
    public String class_desc;
    public String class_status;
    public List<String> strList;
    public String token;
    public String create_user;
}

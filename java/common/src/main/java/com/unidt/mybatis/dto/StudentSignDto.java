package com.unidt.mybatis.dto;

public class StudentSignDto {
    public String student_id;
    public String lesson_id;
    public String token;
    public String create_user;
}

package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.TeacherCourseInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface TeacherCourseMapper {

    @Select("select * from tab_teacher_course where teacher_course_id = #{id}")
    public TeacherCourseInfo selectTeacherCourseID(String id);

    @Select("select * from tab_teacher_course where teacher_id = #{id}")
    public TeacherCourseInfo selectCourseByTeacherID(String id);

    @Insert("insert into tab_teacher_course values(#{teacher_course_id},#{teacher_id},#{course_id},#{start_date}," +
    "#{delexpire_date_flag})")
    public void insertTeacherCourse(TeacherCourseInfo info);

    @Update("update tab_teacher_course set teacher_id = #{teacher_id}, course_id = #{course_id}," +
            "start_date = #{start_date}, end_date = #{end_date} where teacher_course_id = #{teacher_course_id}")
    public void updateTeacherCourse(TeacherCourseInfo info);

    @Delete("delete from tab_teacher_course where teacher_course_id=#{id}")
    public void deleteTeacherCourse(String id);

}


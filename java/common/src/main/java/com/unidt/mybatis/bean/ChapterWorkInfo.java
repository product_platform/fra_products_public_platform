package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChapterWorkInfo {
    public String id;
    public String chapter_id;
    public String question_content;
    public char question_type;
    public double question_score;
    public Integer sort;
    public char del_flag;
    public String create_time;
    public String create_user;
}

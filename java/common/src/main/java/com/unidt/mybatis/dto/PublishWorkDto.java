package com.unidt.mybatis.dto;

public class PublishWorkDto {
    public String work_desc = null;
    public String course_id = null;
    public String work_url = null;
    public String chapter_id = null;
    public String work_name = null;
    public String token = null;
}

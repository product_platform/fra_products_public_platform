package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentInfo {
    public String student_id;
    public String student_no;
    public String student_name;
    public String school_id;
    public String grade_id;
    public String class_id;
    public char studen_sex;
    public String studen_age;
    public String student_mail;
    public String student_phone;
    public String login_user;
    public String login_pwd;
    public char del_flag;
    public String create_user;
    public String create_time;
    public String update_user;
    public String update_time;
}























package com.unidt.mybatis;

import com.unidt.mybatis.bean.ClassInfo;
import com.unidt.mybatis.bean.SSO;
import com.unidt.mybatis.bean.TeacherInfo;
import com.unidt.mybatis.mapper.ClassInfoMapper;
import com.unidt.mybatis.mapper.SSOMapper;
import com.unidt.mybatis.mapper.TeacherInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@ComponentScan

public class TeacherTest {
    @Autowired
    TeacherInfoMapper teacherInfoMapper;
    @Autowired
    ClassInfoMapper classInfoMapper;
    @Autowired
    SSOMapper       ssoMapper;


    @RequestMapping(value = "/teacher", method = RequestMethod.GET)
    public String getTeacherByID(String id) {
        System.out.println("Get Teacher, ID = " + id);
        TeacherInfo info = teacherInfoMapper.selectTeacherByID(id);
        System.out.println(info.teacher_name);
        return info.teacher_name;
    }

    @RequestMapping(value = "/classinfo", method = RequestMethod.GET)
    public String getClass(String id) {
        System.out.println("Get Class: " + id);
        ClassInfo info = classInfoMapper.selectClassByID(id);
        return info.class_desc;
    }

    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public String token(String token) {
        if (check_token(token)) {
            return "OK";
        }else {
            return "token 无效";
        }
    }

    public boolean check_token(String token) {
        SSO so = ssoMapper.getSSOByToken(token);
        if ( so == null) {
            return false;
        }
        return true;
    }

}

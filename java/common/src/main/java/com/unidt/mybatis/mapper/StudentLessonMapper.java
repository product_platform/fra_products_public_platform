package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.ChapterInfo;
import com.unidt.mybatis.bean.StudentLessonInfo;
import com.unidt.mybatis.dto.CourseLessonDto;
import org.apache.ibatis.annotations.*;

@Mapper
public interface StudentLessonMapper {

    @Select("select * from tab_student_lesson where student_lesson_id = #{id}")
    public StudentLessonInfo selectStudentLessonByID(String id);

    @Insert("insert into tab_student_lesson values(#{student_lesson_id},#{student_id},#{lesson_id},#{signin_time},#{signout_time},#{del_flag})")
    public void insertStudentLesson(StudentLessonInfo info);

    @Update("update tab_student_lesson set student_id = #{student_id}, lesson_id = #{lesson_id}," +
            "signin_time = #{signin_time}, signout_time = #{signout_time}, del_flag = #{del_flag} where student_lesson_id = #{student_lesson_id}")
    public void updateStudentLesson(StudentLessonInfo info);

    @Delete("delete from tab_student_lesson where student_lesson_id=#{id}")
    public void deleteStudentLesson(String id);


    /**
     *  获取当前课程学生签到总人数
     * @param lesson_id
     * @return
     */
    @Select("select COUNT( DISTINCT i.student_id) all_count from tab_student_lesson i where i.lesson_id =  #{lesson_id}")
    public Integer getSumStudentByLesson(String lesson_id);


    @Select("select  count(s.student_lesson_id) as checkCount, count(l.lesson_id) as lessonCount from tab_lesson_info l " +
            "  LEFT JOIN tab_student_lesson s ON s.lesson_id = l.lesson_id" +
            " where l.del_flag= 0 and l.class_id = #{class_id} and l.course_id = #{course_id} " +
            " and l.teacher_id = #{teacher_id} and l.del_flag='0'" +
            " and s.student_id = #{student_id}" +
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  >= DATE_FORMAT(#{start_date}, '%Y-%m-%d')" +
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') <=  DATE_FORMAT(#{end_date}, '%Y-%m-%d')" )
    public CourseLessonDto getCountLessonStudent(CourseLessonDto courseLessonDto);

}


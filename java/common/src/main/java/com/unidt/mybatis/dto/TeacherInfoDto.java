package com.unidt.mybatis.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 教师信息表
 */
@Getter
@Setter
public class TeacherInfoDto {
    /**
     * 教师id
     */
    public String teacher_id;
    /**
     * 教师编号
     */
    public String teacher_no;
    /**
     * 教师密码
     */
    public String teacher_pwd;
    /**
     * 教师姓名
     */
    public String teacher_name;
    /**
     * 教师手机号
     */
    public String teacher_phone;
    /**
     * 教师邮箱
     */
    public String teacher_mail;
    /**
     * 教师级别
     */
    public String teacher_grade;
    /**
     * 所在学校
     */
    public String school_id;
    /**
     * 教师状态
     */
    public String teacher_status;
    /**
     * 教师层级
     */
    public char teacher_level;
    /**
     * 删除标识  0 未删除，1 已删除
     */
    public char del_flag;
    /**
     * 创建人
     */
    public String create_user;
    /**
     * 创建时间
     */
    public String create_time;
    /**
     * 最后更新人
     */
    public String update_user;
    /**
     * 最后更新时间
     */
    public String update_time;
}























package com.unidt.mybatis.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


public class CourseLessonDto {

    /**
     *   用户id
     */
     public String  id;
    /**
     *  开始时间
     */
    public String  start_date;
    /**
     * 结束时间
     */
     public String  end_date;
    /**
     * 显示数量
     */
    public int num ;
    /**
     * 角色编号
     */
        public String role_no;

    /**
     * 班级id
     */
    public String class_id;
    /**
     * 班级名称
     */
    public String class_name;
    /**
     *  科目id
     */
    public String course_id;
    /**
     * 科目名称
     */
    public String course_name;
    /**
     * token
     */
    public String token;
    /**
     * 课程id
     */
    public String lesson_id;
    /**
     * 教师id
     */
    public String teacher_id;
    /**
     * 上课日期
     */
    public String lesson_date;
    /**
     * 上课开始时间
     */
    public String class_start_time;
    /**
     * 上课结束时间
     */
    public String class_end_time;
    /**
     * 课程权限
     */
    public String lesson_right;
    /**
     * 进度状态
     */
    public String schedule_status;

    /**
     * 状态列表
     */
    public List<String> statusList;
    /**
     * 科目列表
     */
    public List<String> courseList;

    /**
     * 班级列表
     */
    public List<String> classList;

    public String  create_user;


    /**
     * 学生id
     */
    public String student_id;


    /**
     * 学生签到数
     */
    public int checkCount;
    /**
     * 课程数
     */
    public int lessonCount;



}

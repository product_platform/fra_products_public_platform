package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.ChapterResourcesInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ChapterResourcesInfoMapper {

    @Select("select * from tab_chapter_resources where resource_id = #{id}")
    public ChapterResourcesInfo selectChapterResourcesByID(String id);

    @Select("select * from tab_chapter_resources where chapter_id = #{id} and del_flag = 0")
    public List<ChapterResourcesInfo> selectChapterResourcesByChapterID(String id);

    @Insert("insert into tab_chapter_resources values(#{resource_id},#{resource_name},#{resource_desc},#{resource_type},#{resource_url},#{resource_right}," +
    "#{resource_size}, #{resource_status}, #{chapter_id},#{del_flag},#{create_user}, #{create_time}, #{update_user},#{update_time},#{resource_cover})")
    public void insertChapterResources(ChapterResourcesInfo info);

    @Update("update tab_chapter_resources set resource_name = #{resource_name}, resource_desc = #{resource_desc},resource_cover = #{resource_cover}," +
            "resource_type = #{resource_type}, resource_url = #{resource_url}, resource_right = #{resource_right}," +
            "resource_size = #{resource_size}, resource_status = #{resource_status}, chapter_id = #{chapter_id}, del_flag = #{del_flag}," +
            "create_user = #{create_user}, create_time = #{create_time}, update_user = #{update_user}, update_time = #{update_time} where resource_id = #{resource_id}")
    public void updateChapterResources(ChapterResourcesInfo info);

    @Delete("delete from tab_chapter_resources where resource_id=#{id}")
    public void deleteChapterResources(String id);

}


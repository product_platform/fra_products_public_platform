package com.unidt.mybatis.bean;

public class StudentLessonInfo {
    public String student_lesson_id;
    public String student_id;
    public String lesson_id;
    public String signin_time;
    public String signout_time;
    public String del_flag;
}

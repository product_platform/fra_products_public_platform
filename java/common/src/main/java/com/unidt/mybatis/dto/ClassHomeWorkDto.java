package com.unidt.mybatis.dto;

public class ClassHomeWorkDto {
    public String teacher_id;
    public String course_id;
    public String schedule_status;
    public String[] class_id;
    public String date;
    public String token;
    public String create_user;
}

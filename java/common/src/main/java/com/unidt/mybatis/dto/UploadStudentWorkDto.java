package com.unidt.mybatis.dto;

public class UploadStudentWorkDto {
    public String student_work_id;
    public String work_id;
    public String lesson_id;
    public String lesson_chapter_id;
    public String content;
    public String work_url;
    public String student_work_url;
    public String token;
    public String create_user;
}

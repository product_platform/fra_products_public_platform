package com.unidt.mybatis.dto;

public class CopyLessonChapterDto {
    public String class_id;
    public String teacher_id;
    public String course_id;
    public String lesson_date;
    public String class_start_time;
    public String[] chapterList;
    public String token;
    public String start_date = null;
    public String end_date = null;
    public String create_user;
}

package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.TeacherResourcesInfo;
import com.unidt.mybatis.dto.UploadDto;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TeacherResourcesMapper {

    @Select("select * from tab_teacher_resources where teacher_resource_id = #{id}")
    public TeacherResourcesInfo selectTeacherResourcesByID(String id);

    @Select("select chapter_id,teacher_resource_id,resource_name,resource_size,resource_type,last_play_time," +
            "resource_url,resource_cover from tab_teacher_resources where chapter_id = #{id} and del_flag = 0")
    public List<TeacherResourcesInfo> selectTeacherResourcesByChapterID(String id);

    @Insert("insert into tab_teacher_resources(teacher_resource_id, lesson_chapter_id, resource_name, resource_desc,resource_cover, resource_type, last_play_time," +
            "resource_right, resource_url, resource_size, teacher_id, chapter_id, del_flag, create_user, create_time, update_user, update_time) " +
            "values(#{teacher_resource_id},#{lesson_chapter_id},#{resource_name},#{resource_cover}," +
            "#{resource_desc},#{resource_type},#{last_play_time},#{resource_right},#{resource_url}," +
            "#{resource_size},#{teacher_id},#{chapter_id},#{del_flag}," +
            "#{create_user},#{create_time},#{update_user},#{update_time})")
    public void insertTeacherResources(TeacherResourcesInfo info);

    @Update("update tab_teacher_resources set lesson_chapter_id = #{lesson_chapter_id}, last_play_time = #{last_play_time},resource_cover = #{resource_cover}," +
            "resource_name=#{resource_name}, resource_desc= #{resource_desc}, resource_type=#{resource_type}, resource_right= #{resource_right}," +
            "resource_url=#{resource_url}, resource_size= #{resource_size}, teacher_id=#{teacher_id}, chapter_id= #{chapter_id}," +
            "del_flag=#{del_flag}, update_user=#{update_user}, update_time= #{update_time} where teacher_resource_id = #{teacher_resource_id}")
    public void updateTeacherResources(TeacherResourcesInfo info);

    @Update("update tab_teacher_resources set del_flag = 1 where teacher_resource_id=#{id}")
    public void deleteTeacherResourcesByID(String id);

}


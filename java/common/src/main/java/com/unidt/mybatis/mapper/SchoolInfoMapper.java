package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.SchoolInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface SchoolInfoMapper {

    @Select("select * from tab_school_info where school_id = #{id}")
    public SchoolInfo selectSchoolByID(String id);

    @Insert("insert into tab_school_info values(#{school_id},#{school_no},#{school_name},#{school_type},#{school_desc},#{school_status}," +
    "#{school_address}, #{del_flag}, #{school_user},#{school_phone})")
    public void insertSchool(SchoolInfo info);

    @Update("update tab_school_info set school_no = #{school_no}, school_name = #{school_name}," +
            "school_type = #{school_type}, school_desc = #{school_desc}, school_status = #{school_status}," +
            "school_address = #{school_address}, del_flag = #{del_flag}, school_user = #{school_user}, school_phone = #{school_phone} where school_id = #{school_id}")
    public void updateSchool(SchoolInfo info);

    @Delete("delete from tab_school_info where school_id=#{id}")
    public void deleteSchool(String id);

}


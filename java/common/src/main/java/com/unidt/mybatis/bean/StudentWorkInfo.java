package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentWorkInfo {
    public String student_work_id;
    public String student_id;
    public String work_id;
    public String lesson_id;
    public String update_time;
    public Integer score;
    public String work_status;
    public String lesson_chapter_id;
    public String content;
    public String work_url;
    public String student_work_url;
}

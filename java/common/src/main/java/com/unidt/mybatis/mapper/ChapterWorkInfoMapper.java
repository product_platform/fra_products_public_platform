package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.ChapterWorkInfo;
import com.unidt.mybatis.bean.LessonChapterInfo;
import com.unidt.mybatis.dto.CorrectWorkDto;
import com.unidt.mybatis.dto.HomeWorkContentDto;
import com.unidt.mybatis.dto.WorkContentAnswerDto;
import org.apache.ibatis.annotations.*;
import java.util.List;

@Mapper
public interface ChapterWorkInfoMapper {

    @Select("select * from tab_chapter_work where id = #{id}")
    public ChapterWorkInfo selectChapterWorkByID(String id);

    @Select("SELECT chapter_id FROM tab_chapter_work WHERE create_time LIKE '#{date}%'")
    public List<ChapterWorkInfo> selectChapterIDWorkByDate(String date);

    @Select("select id from tab_chapter_work where del_flag = 0 and chapter_id in (select chapter_id from tab_lesson_chapter where lesson_id = #{lesson_id} and del_flag = 0)")
    public List<String> selectChapterWorkByLessonID(String lesson_id);

    /**
     *  根据课程获取课程下章节
     * @param id
     * @return
     */
    @Select("select c.* from tab_lesson_chapter c where c.lesson_id = #{id} and c.del_flag = 0")
    public List<LessonChapterInfo> selectChapterLessonId(String id);

    /**
     *  根据章节id获取作业列表
     * @param id
     * @return
     */
    @Select("select w.* from tab_chapter_work w where w.chapter_id = #{id} and w.del_flag = 0")
    public List<ChapterWorkInfo> selectWorkByChapterId(String id);

    @Select("select * from tab_chapter_work where chapter_id = #{chapter_id} and del_flag = 0")
    public ChapterWorkInfo selectChapterWorkByChapterID(String id);

    @Select("SELECT" +
            "    a.question_content," +
            "    a.question_type," +
            "    b.content," +
            "    b.is_correct_answer," +
            "    b.order_sort," +
            "    b.id" +
            "    from  tab_chapter_work_answer  as b left join tab_chapter_work as a   on a.id = b.chapter_work_id" +
            "    where a.del_flag = 0 and  a.id = #{id} ")
    public List<WorkContentAnswerDto> selectChapterWorkContentByWorkID(String id);


    @Select("select a.question_content,a.question_type,b.content,b.is_correct_answer,b.order_sort,b.id ,c.content as student_answer,c.score \n" +
            "from tab_student_work c LEFT JOIN tab_chapter_work a on c.work_id=a.id " +
            "left join tab_chapter_work_answer b on a.id = b.chapter_work_id " +
            " where c.work_id=#{work_id} and c.student_id=#{student_id} and c.lesson_chapter_id=#{lesson_chapter_id}")
    public List<WorkContentAnswerDto> selectStudentWorkContentByWorkID(HomeWorkContentDto homeWorkContentDto);

    @Select("select a.question_type,a.question_score,b.content from tab_chapter_work a" +
            " left join tab_chapter_work_answer b on a.id = b.chapter_work_id" +
            " where a.id = #{id} and b.is_correct_answer = 1 and a.del_flag = 0")
    public CorrectWorkDto selectWorkAnswerByID(String id);


    @Insert("insert into tab_chapter_work values(#{id},#{chapter_id},#{question_content},#{question_type},#{question_score},#{sort}," +
    "#{del_flag}, #{create_time},#{create_user})")
    public void insertChapterWork(ChapterWorkInfo info);

    @Update("update tab_chapter_work set chapter_id = #{chapter_id}," +
            "question_content = #{question_content}, question_type = #{question_type}, question_score = #{question_score}," +
            "sort = #{sort}, del_flag = #{del_flag}, create_time = #{create_time}, create_user = #{create_user} where id = #{id}")
    public void updateChapterWork(ChapterWorkInfo info);

    @Delete("delete from tab_chapter_work where id=#{id}")
    public void deleteChapterWork(String id);

}


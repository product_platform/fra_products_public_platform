package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.ClassInfo;
import com.unidt.mybatis.bean.WorkInfo;
import com.unidt.mybatis.dto.ContentDto;
import org.apache.ibatis.annotations.*;

@Mapper
public interface WorkInfoMapper {

    @Select("select * from tab_work_info where work_id = #{id}")
    public WorkInfo selectWorkInfoByID(String id);

    @Select("select * from tab_work_info as a left join tab_chapter_work as b on a.work_id = b.id where work_id = #{id}")
    public ContentDto selectWorkInfoQuestionContentByID(String id);

    @Insert("insert into tab_work_info values(#{work_id},#{work_name},#{work_desc},#{course_id},#{work_url},#{chapter_id})")
    public void insertWorkInfo(WorkInfo info);

    @Update("update tab_work_info set work_name = #{work_name}, work_desc = #{work_desc}," +
            "course_id = #{course_id}, work_url = #{work_url}, chapter_id = #{chapter_id} where work_id = #{work_id}")
    public void updateWorkInfo(WorkInfo info);

    @Delete("delete from tab_work_info where class_id=#{id}")
    public void deleteWorkInfo(String id);

}


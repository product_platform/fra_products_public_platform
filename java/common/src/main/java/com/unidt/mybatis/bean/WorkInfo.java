package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkInfo {
    public String work_id;
    public String work_name;
    public String work_desc;
    public String course_id;
    public String work_url;
    public String chapter_id;
}

package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.ClassInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface ClassInfoMapper {

    @Select("select * from tab_class_info where class_id = #{id}")
    public ClassInfo selectClassByID(String id);

    @Insert("insert into tab_class_info values(#{class_id},#{grade_id},#{grade_name},#{class_name},#{class_desc},#{class_status}," +
    "#{school_id}, #{del_flag})")
    public void insertClass(ClassInfo info);

    @Update("update tab_class_info set grade_id = #{grade_id}, grade_name = #{grade_name}," +
            "class_name = #{class_name}, class_desc = #{class_desc}, class_status = #{class_status}," +
            "school_id = #{school_id}, del_flag = #{del_flag} where class_id = #{class_id}")
    public void updateClass(ClassInfo info);

    @Delete("delete from tab_class_info where class_id=#{id}")
    public void deleteClass(String id);

}


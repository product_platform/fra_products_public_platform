package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseInfo {
    public String course_id;
    public String course_no;
    public String course_name;
    public String course_desc;
    public String course_grade;
    public String course_type;
    public String covermap_url;
    public char del_flag;
    public String course_level_id;
    public String create_time;
}

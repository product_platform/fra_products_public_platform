package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClassInfo {
    public String class_id;
    public String grade_id;
    public String grade_name;
    public String class_name;
    public String class_desc;
    public String class_status;
    public char school_id;
    public char del_flag;
}

package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChapterInfo {
    public String chapter_id;
    public String chapter_name;
    public String chapter_title;
    public String chapter_content;
    public String chapter_level;
    public String course_id;
    public String chapter_parentid;
    public char del_flag;
    public String create_time;
    public String update_time;
}

package com.unidt.mybatis.dto;

public class WorkContentAnswerDto {
    public String question_content;
    public String question_type;
    public String content;
    public String is_correct_answer;
    public String order_sort;
    public String id;
    public String lesson_chapter_id;
    public String student_answer;
    public Integer score;
}

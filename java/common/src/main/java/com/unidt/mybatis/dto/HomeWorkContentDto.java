package com.unidt.mybatis.dto;


public class HomeWorkContentDto {
    public String work_id;
    public String student_id;
    public String lesson_id;
    public String lesson_chapter_id;
    public String student_work_id;
    public String id;
    public String token;
    public String create_user;
}

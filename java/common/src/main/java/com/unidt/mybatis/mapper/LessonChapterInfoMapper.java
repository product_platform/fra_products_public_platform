package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.LessonChapterInfo;
import com.unidt.mybatis.bean.LessonInfo;
import com.unidt.mybatis.dto.CourseLessonDto;
import com.unidt.mybatis.dto.LessonDetail;
import com.unidt.mybatis.dto.UploadDto;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LessonChapterInfoMapper {

    @Select("select * from tab_lesson_chapter where lesson_chapter_id = #{id}")
    public LessonChapterInfo selectGradeByID(String id);

    @Select("select lesson_chapter_id from tab_lesson_chapter where lesson_id = #{lesson_id} and chapter_id = #{chapter_id}")
    public String selectLCIDByLessonIDChapterID(UploadDto uploadDto);

    @Select("select chapter_id from tab_lesson_chapter where lesson_id = " +
            "(select lesson_id from tab_lesson_info where course_id = #{course_id} and lesson_date = #{lesson_date})")
    public List<LessonChapterInfo> selectChapterIDByLesson(LessonInfo info);

    @Select("select c.chapter_id from tab_lesson_chapter c where c.lesson_id = #{lesson_id}")
    public List<String> getLessonChapterById(String lesson_id);

    @Select("select DISTINCT c.chapter_id from tab_lesson_chapter c " +
            " LEFT JOIN tab_lesson_info l ON l.lesson_id = c.lesson_id" +
            " LEFT JOIN tab_chapter_info p on p.chapter_id = c.chapter_id where" +
            "l.class_id = #{class_id} and l.course_id =#{course_id} and l.del_flag =0")
    public List<String> getCourseChapterByClass(CourseLessonDto courseLessonDto);

    @Select("select c.* from tab_lesson_chapter c where c.lesson_id = #{lesson_id} and c.del_flag=0")
    public List<LessonChapterInfo> getChapterListByLessonId(String lesson_id);

    @Insert("insert into tab_lesson_chapter (lesson_chapter_id,chapter_id,last_play_time,chapter_parent_id,lesson_id,status,del_flag) values(#{lesson_chapter_id},#{chapter_id},#{last_play_time},#{chapter_parent_id},#{lesson_id}," +
            "#{status}, #{del_flag})")
    public void insertGrade(LessonChapterInfo info);

    @Update("update tab_lesson_chapter set chapter_id = #{chapter_id}, chapter_parent_id = #{chapter_parent_id}," +
            "lesson_id = #{lesson_id}, status = #{status}, del_flag = #{del_flag} where lesson_chapter_id = #{lesson_chapter_id}")
    public void updateGrade(LessonChapterInfo info);

    @Update("update tab_lesson_chapter set chapter_id = #{chapter_id} where lesson_id = #{lesson_id}")
    public void updateChapterByLessonID(LessonChapterInfo info);

    @Delete("delete from tab_lesson_chapter where lesson_chapter_id=#{id}")
    public void deleteGrade(String id);

    @Delete("delete from tab_lesson_chapter where lesson_id=#{id}")
    public void deleteByLesson(String id);

    @Delete({"<script>",
            "delete from tab_lesson_chapter  where lesson_id = #{lesson_id}",
            "<when test='ListId != null and ListId.size() > 0'>",
            "AND chapter_id not in"+
                    "<foreach collection='ListId' index='index' item='item' open='(' separator=',' close=')'>"+
                    "#{item}"+
                    "</foreach>",
            "</when>",
            "</script>"})
    public void deleteByList(@Param("ListId") List<String> ListId, @Param("lesson_id") String lesson_id);


    @Select("select a.lesson_id, a.chapter_id, b.chapter_name,b.chapter_title from tab_lesson_chapter a " +
            "left join tab_chapter_info b on a.chapter_id = b.chapter_id " +
            "where a.lesson_id= #{id} and a.del_flag = 0 and b.del_flag = 0")
    public List<LessonDetail> getLessonDetail(String id);

}

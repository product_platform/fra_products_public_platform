package com.unidt.mybatis.dto;

public class UploadDto {
    public String lesson_chapter_id = null;
    public String resource_desc = null;
    public String resource_type = null;
    public String resource_right = null;
    public String last_play_time = null;
    public String resource_cover = null;
    public String teacher_id = null;
    public String chapter_id = null;
    public String resource_name;
    public String resource_url;
    public String token;
    public String teacher_resource_id;
    public char del_flag;
    public String create_user = null;
    public String create_time = null;
    public String update_time = null;
    public String lesson_id;
    public int flag;
    public String filepath;

}

package com.unidt.mybatis.dto;

import com.unidt.mybatis.bean.LessonChapterInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class LessonChapterDto {
    /**
     * 课程
     */
    public String lesson_id;

    public List<String> chapterList;
    /**
     * 课程章节id
     */
    public String lesson_chapte_id;
    /**
     * 章节名称
     */
    public String chapter_name;
    /**
     * 章节标题
     */
    public String chapter_title;

    public String chapter_id;

    public String chapter_parent_id;

    public String status;

    public String create_user;

    public String token;

}

package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.GradeInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface GradeInfoMapper {

    @Select("select * from tab_grade where grade_id = #{id}")
    public GradeInfo selectGradeByID(String id);

    @Insert("insert into tab_grade values(#{grade_id},#{grade_name},#{sort},#{type}," +
    "#{del_flag})")
    public void insertGrade(GradeInfo info);

    @Update("update tab_grade set grade_name = #{grade_name}, sort = #{sort}," +
            "type = #{type}, del_flag = #{del_flag} where grade_id = #{grade_id}")
    public void updateGrade(GradeInfo info);

    @Delete("delete from tab_grade where grade_id=#{id}")
    public void deleteGrade(String id);

}


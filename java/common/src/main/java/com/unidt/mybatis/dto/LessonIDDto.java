package com.unidt.mybatis.dto;

public class LessonIDDto {
    public String lesson_id;
    public String student_id;
    public String token;
    public String create_user;
}

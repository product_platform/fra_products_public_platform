package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LessonInfo {
    public String lesson_id;
    public String teacher_id;
    public String course_id;
    public String lesson_date;
    public String class_start_time;
    public String class_end_time;
    public String play_chapter_node;//最后播放章节
    public String play_resource_node;//最后播放资源点
    public String class_id;
    public Integer student_num;
    public String lesson_right;
    public String schedule_status;
    public String update_time;
    public String update_user;
    public String create_time;
    public String start_date;
    public String create_user;
    public String end_date;
    public String del_flag;

}

package com.unidt.mybatis.dto;

public class LessonChapterIDDto {
    public String work_id;
    public String lesson_id;
    public String lesson_chapter_id;
    public String student_id;
    public String token;
    public String create_user;
}

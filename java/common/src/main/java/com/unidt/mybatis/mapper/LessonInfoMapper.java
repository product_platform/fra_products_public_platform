package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.TeacherInfo;
import com.unidt.mybatis.bean.LessonInfo;
import com.unidt.mybatis.dto.*;
import org.apache.ibatis.annotations.*;
import java.util.ArrayList;
import java.util.List;

@Mapper
public interface LessonInfoMapper {

    @Select("select * from tab_lesson_info where teacher_id = #{id}")
    public List<TeacherInfo> selectLessonByTeacherID(String id);

    @Select("select * from tab_lesson_info a left join tab_course_info b on a.course_id = b.course_id where class_id = #{id}")
    public List<LessonInfoDto> selectLessonByClassID(String class_id);

    @Select("select lesson_id from tab_lesson_info where lesson_date = #{lesson_date} and class_start_time = #{class_start_time} and del_flag = 0")
    public String selectLessonIDByStartTime(LessonInfo lessonInfo);

    @Select("select DISTINCT(a.course_id),b.course_name from tab_lesson_info a left join tab_course_info b on a.course_id = b.course_id where a.class_id = #{id} and a.del_flag = 0")
    public List<LessonInfoDto> selectCourseByClassID(String class_id);

    @Select("select * from tab_lesson_info where lesson_id = #{id}")
    public LessonInfo selectLessonByID(String id);


    @Select("select * from tab_lesson_info as a left join tab_course_info as b on " +
            "a.course_id = b.course_id where a.teacher_id = #{teacher_id} and a.course_id = #{course_id} and a.del_flag =0")
    public List<LessonCourseInfoDto> selectLessonByMulti(LessonCourseInfoDto info);

    @Select("select l.* from tab_lesson_info l where l.teacher_id = #{teacher_id} and l.class_id = #{class_id} and l.course_id = #{course_id} " +
            "and l.lesson_date = #{lesson_date} and l.class_start_time = #{class_start_time} and l.del_flag = 0")
    public LessonInfo selectLessonInfoByMulti(CopyLessonChapterDto info);

    @Insert("INSERT INTO tab_lesson_info (lesson_id, teacher_id, course_id, lesson_date,class_start_time,class_id,student_num," +
            "lesson_right,schedule_status,create_time,start_date,create_user,end_date,del_flag)" +
            "VALUES (#{lesson_id},#{teacher_id},#{course_id},#{lesson_date},#{class_start_time},#{class_id},#{student_num}," +
            "#{lesson_right},#{schedule_status},#{create_time},#{start_date}, #{create_user},#{end_date}," +
            "#{del_flag})")
    public void insertLesson(LessonInfo info);

    @Update("update tab_lesson_info set teacher_id = #{teacher_id}, course_id = #{course_id}, lesson_date = #{lesson_date}," +
            "class_start_time = #{class_start_time}, class_end_time = #{class_end_time},class_id = #{class_id}," +
            "student_num = #{student_num}, lesson_right = #{lesson_right}, schedule_status = #{schedule_status}, update_time = #{update_time}" +
            "update_user = #{update_user}, lesson_right = #{lesson_right}, schedule_status = #{schedule_status} where lesson_id = #{lesson_id}")
    public void updateLesson(LessonInfo info);

    @Delete("delete from tab_lesson_info where lesson_id=#{id}")
    public void deleteLesson(String id);


    @Select("select DISTINCT teacher_id,a.class_id,b.class_name,a.course_id, c.course_name from tab_lesson_info a " +
            "left join tab_class_info b on a.class_id=b.class_id " +
            "left join tab_course_info c on a.course_id=c.course_id where a.teacher_id = #{id} ")
    public List<TeacherClassCourse> getTeacherCourseClass(String id);

    @Select("select DISTINCT a.teacher_id, c.course_id, c.course_desc, c.course_grade, c.covermap_url, " +
            "c.course_type,c.course_name from tab_teacher_course a " +
            "left join tab_course_info c on a.course_id=c.course_id where a.teacher_id = #{id}  ")
    public List<TeacherClassCourse> getTeacherCourseInfo(String id);


    @Select("select DISTINCT teacher_id, a.class_id, b.class_name, b.grade_id, b.grade_name ,b.class_desc,b.class_status from tab_teacher_class a " +
            "left join tab_class_info b on a.class_id=b.class_id where a.teacher_id = #{id} and  b.del_flag = 0")
    public List<TeacherClassCourse> getTeacherClassInfo(String id);


    @Select("select l.lesson_id, l.class_id, l.teacher_id, l.course_id, l.lesson_date, l.class_start_time, " +
            "l.class_end_time, l.lesson_right, l.schedule_status, c.class_name, o.course_name, o.course_type, o.covermap_url from tab_lesson_info l " +
            "left join tab_class_info c on c.class_id = l.class_id " +
            "left join tab_course_info o on o.course_id = l.course_id " +
            "where l.teacher_id = #{id} and  l.del_flag = 0 order by  l.lesson_date,l.class_start_time ")
    public ArrayList<LessonInfoDto> getCourseLessonList(String id);

    @Select("select l.lesson_id,l.class_id, l.teacher_id, l.course_id, l.lesson_date, l.class_start_time, " +
            "l.class_end_time, l.lesson_right, l.schedule_status, c.class_name, o.course_name, o.course_type, o.covermap_url from tab_lesson_info l " +
            "left join tab_class_info c on c.class_id = l.class_id " +
            "left join tab_course_info o on o.course_id = l.course_id " +
            "where l.teacher_id = #{teacher_id} and l.schedule_status = #{status} and  l.del_flag = 0 order by  l.lesson_date,l.class_start_time ")
    public ArrayList<LessonInfoDto> getCourseLessonListBystatus(@Param("teacher_id") String teacher_id, @Param("status") String status);

    @Select("select l.lesson_id,l.class_id, l.teacher_id, l.course_id, l.lesson_date, l.class_start_time, " +
            "l.class_end_time, l.lesson_right, l.schedule_status, c.class_name, o.course_name from tab_lesson_info l " +
            "left join tab_class_info c on c.class_id = l.class_id " +
            "left join tab_course_info o on o.course_id = l.course_id " +
            "where l.teacher_id = #{id} and l.class_id = #{class_id} and l.course_id = #{course_id} and l.del_flag='0' " +
            "AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  >= DATE_FORMAT(#{start_date}, '%Y-%m-%d')" +
            "AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') <=  DATE_FORMAT(#{end_date}, '%Y-%m-%d') order by  l.lesson_date,l.class_start_time" )
    public ArrayList<LessonInfoDto> selectTeacherLessonByInfo(CourseLessonDto info);


    @Select("select l.lesson_id, l.class_id, l.teacher_id, l.course_id, l.lesson_date, l.class_start_time, " +
            "l.class_end_time, l.lesson_right, l.schedule_status, c.class_name, o.course_name from tab_lesson_info l " +
            "left join tab_class_info c on c.class_id = l.class_id " +
            "left join tab_course_info o on o.course_id = l.course_id " +
            "where l.class_id = #{class_id} and l.course_id = #{course_id} and l.del_flag='0' " +
            "AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  >= DATE_FORMAT(#{start_date}, '%Y-%m-%d')" +
            "AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') <=  DATE_FORMAT(#{end_date}, '%Y-%m-%d') order by  l.lesson_date,l.class_start_time " )
    public ArrayList<LessonInfoDto> selectStudentLessonByInfo(CourseLessonDto info);


    /**老师获取日程*/
    @Select({"<script>",
            "select l.lesson_id, l.class_id, l.teacher_id, l.course_id, l.lesson_date, l.class_start_time,"  +
                    " l.class_end_time, l.lesson_right, l.schedule_status, c.class_name," +
                    " l.play_chapter_node, l.play_resource_node," +
                    " o.course_name,o.course_type, o.covermap_url from tab_lesson_info l" +
                    " left join tab_class_info c on c.class_id = l.class_id " +
                    " left join tab_course_info o on o.course_id = l.course_id " +
                    " where l.del_flag= 0  and l.teacher_id=#{id}",
            "<when test='course_id != null'>" ,
            " and l.course_id = #{course_id}",
            "</when>",
            "<when test='class_id != null'>" ,
            " and l.class_id = #{class_id}",
            "</when>",
            "<when test='statusList != null and statusList.size() > 0'>",
            "AND l.schedule_status in"+
                    "<foreach collection='statusList' index='index' item='item1' open='(' separator=',' close=')'>"+
                    "#{item1}"+
                    "</foreach>",
            "</when>",
            "<when test='courseList != null and courseList.size() > 0'>",
            "AND l.course_id in"+
                    "<foreach collection='courseList' index='index' item='item2' open='(' separator=',' close=')'>"+
                    "#{item2}"+
                    "</foreach>",
            "</when>",
            "<when test='classList != null and classList.size() > 0'>",
            "AND l.class_id in"+
                    "<foreach collection='classList' index='index' item='item3' open='(' separator=',' close=')'>"+
                    "#{item3}"+
                    "</foreach>",
            "</when>",
            "<when test='schedule_status != null'>" ,
            " and l.schedule_status = #{schedule_status}",
            "</when>",
            "<when test='start_date != null'>" ,
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  &gt;= DATE_FORMAT(#{start_date}, '%Y-%m-%d')",
            "</when>",
            "<when test='end_date != null'>" ,
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') &lt;=  DATE_FORMAT(#{end_date}, '%Y-%m-%d')",
            "</when>",
            " order by  l.lesson_date,l.class_start_time",
            "</script>"})
    public ArrayList<LessonInfoDto> getTeacherLessonList(CourseLessonDto courseLessonDto);

    /**学生获取日程*/
    @Select({"<script>",
            "select l.lesson_id, l.class_id, l.teacher_id, l.course_id, l.lesson_date, l.class_start_time,"  +
                    " l.class_end_time, l.lesson_right, l.schedule_status,l.play_chapter_node," +
                    "l.play_resource_node, c.class_name, o.course_name, o.course_type, o.covermap_url  from tab_lesson_info l" +
                    " left join tab_class_info c on c.class_id = l.class_id " +
                    " left join tab_course_info o on o.course_id = l.course_id " +
                    " where l.del_flag= 0 and l.class_id = #{class_id}",
            "<when test='course_id != null'>" ,
            " and l.course_id = #{course_id}",
            "</when>",
            "<when test='statusList != null and statusList.size() > 0'>",
            "AND  l.schedule_status in"+
                    "<foreach collection='statusList' index='index' item='item1' open='(' separator=',' close=')'>"+
                    "#{item1}"+
                    "</foreach>",
            "</when>",
            "<when test='schedule_status != null'>" ,
            " and l.schedule_status = #{schedule_status}",
            "</when>",
            "<when test='courseList != null and courseList.size() > 0'>",
            "AND l.course_id in"+
                    "<foreach collection='courseList' index='index' item='item2' open='(' separator=',' close=')'>"+
                    "#{item2}"+
                    "</foreach>",
            "</when>",
            "<when test='start_date != null'>" ,
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  &gt;= DATE_FORMAT(#{start_date}, '%Y-%m-%d')",
            "</when>",
            "<when test='end_date != null'>" ,
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') &lt;=  DATE_FORMAT(#{end_date}, '%Y-%m-%d')",
            "</when>",
            " order by  l.lesson_date,l.class_start_time",
            "</script>"})
    public ArrayList<LessonInfoDto> getStudentLessonList(CourseLessonDto courseLessonDto);


    @Select("select count(w.id) from tab_lesson_chapter c " +
            " LEFT JOIN tab_lesson_info l ON l.lesson_id = c.lesson_id " +
            " LEFT JOIN tab_chapter_work w ON w.chapter_id = c.chapter_id" +
            " where  l.del_flag= 0 and l.class_id = #{class_id} and l.course_id = #{course_id} and l.teacher_id = #{teacher_id} and l.del_flag='0' " +
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  >= DATE_FORMAT(#{start_date}, '%Y-%m-%d')" +
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') <=  DATE_FORMAT(#{end_date}, '%Y-%m-%d')" )
    public Integer getLessonWorkCount(CourseLessonDto courseLessonDto);


    @Select("select  count(s.student_work_id) from tab_lesson_info l " +
            " LEFT JOIN tab_student_work s ON s.lesson_id = l.lesson_id" +
            " where l.del_flag= 0 and l.class_id = #{class_id} and l.course_id = #{course_id} and l.teacher_id = #{teacher_id} and l.del_flag='0' and s.student_id = #{student_id}" +
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d')  >= DATE_FORMAT(#{start_date}, '%Y-%m-%d')" +
            " AND DATE_FORMAT(l.lesson_date, '%Y-%m-%d') <=  DATE_FORMAT(#{end_date}, '%Y-%m-%d')" )
    public Integer getStudentWorkCount(CourseLessonDto courseLessonDto);

    @Select("select count(c.chapter_id) from tab_lesson_chapter c  where c.lesson_id=#{lesson_id}")
    public Integer getLessonIsReady(String lesson_id);


}

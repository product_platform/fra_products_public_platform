package com.unidt.mybatis.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentWorkDto {

    public String lesson_id;
    public String student_id;
    public String student_no;
    public String student_name;
    public String lesson_chapter_id;
    public String work_id;
    public  int    score;
    public  int  sum_score;
    public  int  all_count;
    /**
     *  学生作业id
     */
    public String student_work_id;

}

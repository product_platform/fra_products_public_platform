package com.unidt.mybatis.dto;

public class CorrectWorkDto {
    public String question_type;
    public int question_score;
    public String content;
}

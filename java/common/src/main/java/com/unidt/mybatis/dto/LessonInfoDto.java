package com.unidt.mybatis.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LessonInfoDto implements Serializable {
    /**
     * 科目ID
     */
    public String course_id;
    /**
     * 课程id
     */
    public String lesson_id;
    /**
     * 课程名
     */
    public String course_name;
    /**
     * 课程标签
     */
    public String course_type;
    /**
     * 封面图url
     */
    public String covermap_url;
    /**
     * 班级ID
     */
    public String class_id;
    /**
     * 班级名
     */
    public String class_name;
    /**
     * 最后播放章节
     */
    public String play_chapter_node;
    /**
     * 最后播放资源点
     */
    public String play_resource_node;
    public String teacher_id;
    public String lesson_date;
    public String class_start_time;
    public String class_end_time;
    public String lesson_right;
    public String create_user;
    public String schedule_status;
    public String token;


}

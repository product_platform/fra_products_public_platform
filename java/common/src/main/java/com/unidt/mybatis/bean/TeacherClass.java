package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
/**
 *  教师授课班级表
 */
public class TeacherClass {
    /**
     * 教师班级id
     */
    @NonNull
    public String teacher_class_id;
    /**
     * 教师id
     */
    public String  teacher_id;
    /**
     * 授课班级
     */
    public String  class_id ;
    /**
     * 开始日期
     */
    public String start_date;

    /**
     * 结束日期
     */
    public String end_date;

    /**
     * 删除标识
     */
    public String del_flag;


}

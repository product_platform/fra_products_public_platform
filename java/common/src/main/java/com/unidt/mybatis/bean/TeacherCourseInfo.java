package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherCourseInfo {
    public String teacher_course_id;
    public String teacher_id;
    public String course_id;
    public String start_date;
    public String end_date;
}

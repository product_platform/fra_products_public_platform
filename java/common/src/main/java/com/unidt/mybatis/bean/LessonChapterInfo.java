package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LessonChapterInfo {
    public String lesson_chapter_id;
    public String chapter_id;
    public String chapter_parent_id;
    public String last_play_time;
    public String lesson_id;
    public String status;
    public String del_flag;
}

package com.unidt.mybatis.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LessonDetail {
    public String lesson_id;
    public String chapter_id;
    public String chapter_name;
    public String chapter_title;
}


























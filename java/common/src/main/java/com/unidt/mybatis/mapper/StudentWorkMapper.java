package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.StudentWorkInfo;
import com.unidt.mybatis.dto.LessonChapterIDDto;
import com.unidt.mybatis.dto.StudentWorkDto;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentWorkMapper {

    @Select("select * from tab_student_work where student_work_id = #{id}")
    public StudentWorkInfo selectStudentWorkByID(String id);

    @Select("select lesson_id,a.student_id, b.student_no, b.student_name,work_id, " +
            "score from tab_student_work a left join tab_student_info b on a.student_id = b.student_id" +
            " where lesson_id= #{lesson_id}")
    public List<StudentWorkDto> getAllStudentWorkByLesson(String lesson_id);

    @Select("select lesson_id,a.lesson_chapter_id,a.student_id, b.student_no, b.student_name,work_id, " +
            "score from tab_student_work a left join tab_student_info b on a.student_id = b.student_id" +
            " where work_id= #{work_id} and lesson_id = #{lesson_id}")
    public List<StudentWorkDto> getAllStudentWorkByLessonWork(LessonChapterIDDto lessonChapterIDDto);



    @Select("select lesson_id,a.lesson_chapter_id,a.student_id, b.student_no, b.student_name,work_id, " +
            "score from tab_student_work a left join tab_student_info b on a.student_id = b.student_id" +
            " where a.work_id= #{work_id} and a.lesson_chapter_id = #{lesson_chapter_id} and a.student_id = #{student_id}")
    public List<StudentWorkDto> getLessonoChapyerByLessonWork(LessonChapterIDDto lessonChapterIDDto);


    @Select("select a.lesson_id,a.student_id, b.student_no, b.student_name,a.work_id, " +
            "score from tab_student_work a left join tab_student_info b on a.student_id = b.student_id" +
            " where a.lesson_chapter_id= #{lesson_chapter_id} and a.work_id =#{work_id}")
    public List<StudentWorkDto> getStudentWorkById(@Param("lesson_id") String lesson_id,@Param("work_id") String work_id);


    /**
     *    获取当前课程和当前作业下所有分值和总数
     * @param info
     * @return
     */
    @Select("select  SUM(i.score) as sum_score  ,COUNT( i.student_id) all_count from tab_student_work i where i.lesson_chapter_id= #{lesson_chapter_id}  and i.work_id = #{work_id}")
    public StudentWorkDto getSumAndCountById(StudentWorkInfo info);



    @Select("select * from tab_student_work where student_id = #{student_id} and lesson_chapter_id = #{lesson_chapter_id} and work_id = #{work_id}")
    public StudentWorkInfo selectStudentWorkByMulty(StudentWorkInfo info);

    @Select("select content from tab_student_work where student_id = #{student_id} and work_id = #{work_id}")
    public String selectStudentWorkBySidWid(StudentWorkInfo info);

    @Insert("insert into tab_student_work (student_work_id,student_id,work_id,lesson_id," +
            "score,work_status,lesson_chapter_id,content,work_url,student_work_url)" +
            " values( #{student_work_id},#{student_id},#{work_id},#{lesson_id},#{score},#{work_status}," +
            "#{lesson_chapter_id}, #{content}, #{work_url}, #{student_work_url})")
    public void insertStudentWork(StudentWorkInfo info);

    @Update("update tab_student_work set student_id = #{student_id}, work_id = #{work_id}," +
            "lesson_id = #{lesson_id}, update_time = #{update_time}, score = #{score}, work_status = #{work_status}," +
            "lesson_chapter_id = #{lesson_chapter_id}, content = #{content}, work_url = #{work_url}, student_work_url = #{student_work_url} where student_work_id = #{student_work_id}")
    public void updateStudentWork(StudentWorkInfo info);

    @Delete("delete from tab_student_work where chapter_id=#{id}")
    public void deleteStudentWork(String id);

}


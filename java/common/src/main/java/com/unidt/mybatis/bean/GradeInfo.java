package com.unidt.mybatis.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GradeInfo {
    public String grade_id;
    public String grade_name;
    public int sort;
    public String type;
    public char del_flag;
}

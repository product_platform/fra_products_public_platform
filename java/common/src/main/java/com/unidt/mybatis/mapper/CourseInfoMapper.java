package com.unidt.mybatis.mapper;

import com.unidt.mybatis.bean.CourseInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface CourseInfoMapper {

    @Select("select * from tab_course_info where course_id = #{id}")
    public CourseInfo selectCourseByID(String id);

    @Insert("insert into tab_course_info values(#{course_id},#{course_no},#{course_name},#{course_desc},#{course_grade},#{course_level_id}," +
    "#{create_time}, #{del_flag})")
    public void insertCourse(CourseInfo info);

    @Update("update tab_course_info set course_no = #{course_no}, course_name = #{course_name}," +
            "course_desc = #{course_desc}, course_grade = #{course_grade}, course_level_id = #{course_level_id}," +
            "create_time = #{create_time}, del_flag = #{del_flag} where course_id = #{course_id}")
    public void updateCourse(CourseInfo info);

    @Delete("delete from tab_course_info where course_id=#{id}")
    public void deleteCourse(String id);

}


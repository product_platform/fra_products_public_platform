package com.unidt.api;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.dto.*;
import com.unidt.services.edu.courselesson.CourseLessonService;
import com.unidt.services.edu.courselesson.LessonChapterService;
import com.unidt.services.edu.tools.SSOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 获取科目。班级。课程相关日程
 */
@RestController
@EnableAutoConfiguration
@ComponentScan
public class LessonController {

    @Autowired
    CourseLessonService courseLessonService;
    @Autowired
    LessonChapterService lessonChapterService;

    @Autowired
    SSOService SSOService;

    // 日志
    public static Logger log  = LoggerFactory.getLogger(LessonController.class);

    /**
     * 根据教师id获取教师全部日程
     * @param id
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getTeacherLessonById", method = RequestMethod.GET)
    @ResponseBody
    public  String   getTeacherLessonById(String  id,String status){
        String res ="";
        log.info("查询日程信息 id:" + id);
        res = courseLessonService.getTeacherLessonById(id,status);
        log.info("查询日程信息 res:" + res);
        return res;
    }

    /**
     *   老师根据时间端，班级，科目，查询
     *   （班级，科目。状态均单选，为空时查询全部）
     * @param info
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getTeacherLessonByInfo", method = RequestMethod.POST)
    @ResponseBody
    public  String   getTeacherLessonByInfo(@RequestBody CourseLessonDto info){
        log.info("查询日程信息 class_id:"+info.class_id );
        String token = info.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证时效！").toJson();
            }else{
                info.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return courseLessonService.getTeacherLessonByInfo(info);
    }

    /**
     *  根据班级，科目，时间段获取学生日程
     *  （科目，状态均单选，为空时查询全部）
     *   id 不为空获取教师日程
     * @param info
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getLessonByInfo", method = RequestMethod.POST)
    @ResponseBody
    public  String   getLessonByInfo(@RequestBody CourseLessonDto info){
        log.info("查询日程信息 info:" + info);
         if(info ==null){
             return "";
         }
        String token = info.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                info.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
         return courseLessonService.getLessonByInfo(info);
    }

    /**
     * 教师获取日程
     * @param courseLessonDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getTeacherLesson", method = RequestMethod.POST)
    @ResponseBody
    public  Object   getTeacherLesson(@RequestBody CourseLessonDto courseLessonDto){
        log.info("根据角色添加查询日程信息courseLessonDto:" + courseLessonDto);
        String token = courseLessonDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                courseLessonDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return courseLessonService.getTeacherLesson(courseLessonDto);
    }

    /**
     * 查询学生日程
     * @param courseLessonDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getStudentLesson", method = RequestMethod.POST)
    @ResponseBody
    public  Object   getStudentLesson(@RequestBody CourseLessonDto courseLessonDto){
        log.info("根据角色添加查询日程信息courseLessonDto:" + courseLessonDto);
        String token = courseLessonDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                courseLessonDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return courseLessonService.getStudentLesson(courseLessonDto);
    }

    /**
     * 保存备课信息
     * 根据lesson_id 存储章节id，
     * @param lessonChapterDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/setLessonChapter", method = RequestMethod.POST)
    @ResponseBody
     public Object setLessonChapter(@RequestBody LessonChapterDto lessonChapterDto ){
         log.info("根据角色添加查询日程信息 lessonChapterDto:" + lessonChapterDto);
        String token = lessonChapterDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                lessonChapterDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
         return courseLessonService.setLessonChapter(lessonChapterDto);
     }

    /**
     * 获取课程备课详情
     * @param lessonChapterDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getLessonChapter", method = RequestMethod.POST)
    @ResponseBody
    public Object getLessonChapter(@RequestBody LessonChapterDto lessonChapterDto ){
        log.info("根据角色添加查询日程信息 lessonChapterDto:" + lessonChapterDto);
        String token = lessonChapterDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                lessonChapterDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return courseLessonService.getLessonChapter(lessonChapterDto);
    }

    /**
     * 根据ID删除教师上传资源
     * @param teacherClassCourse
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/delTeacherResource", method = RequestMethod.POST)
    @ResponseBody
    public Object getLessonChapter(@RequestBody TeacherClassCourse teacherClassCourse ){
        Object res="";
        log.info(" /edu/k12/lesson/delTeacherResource 根据ID删除教师上传资源 teacher_resource_id:" + teacherClassCourse.strList);

        String token = teacherClassCourse.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                res =  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
                log.info(" /edu/k12/lesson/delTeacherResource 根据ID删除教师上传资源返回1:" +res);
                return res;
            }else{
                teacherClassCourse.create_user=chackToken.get("user").toString();
            }
        }else{
            res =  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            log.info(" /edu/k12/lesson/delTeacherResource 根据ID删除教师上传资源返回2:" +res);
            return res;
        }
        res = courseLessonService.delTeacherResource(teacherClassCourse);
        log.info(" /edu/k12/lesson/delTeacherResource 根据ID删除教师上传资源返回2:" +res);
        return res;
    }

    /**
     * 科目详情
     */
    @RequestMapping(value = "/edu/k12/lesson/getCourseInfoById", method = RequestMethod.GET)
    @ResponseBody
    public Object getCourseInfoById(String course_id ){
        String res = "";
        log.info("==edu/k12/lesson/getCourseInfoById 获取科目详情 course_id:" + course_id);
        res = courseLessonService.getCourseInfoById(course_id);
        log.info("==edu/k12/lesson/getCourseInfoById 获取科目详情返回:" + res);
        return res;

    }

    /**
     * 根据课程获取章节目录详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getLessChapter", method = RequestMethod.GET)
    @ResponseBody
    public String getLessChapter(String id){
        String res ="";
        log.info("获取章节目录 lesson_id:" + id);
        res = lessonChapterService.getLessonChapterDetail(id);
        log.info("获取章节目录返回"+res);
        return res;
    }

    /**
     * 新增单节排课
     * @param lessonInfoDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/addLessonInfo", method = RequestMethod.POST)
    @ResponseBody
    public String addLessonInfo(@RequestBody LessonInfoDto lessonInfoDto){
        log.info("新增单节排课 lessonInfo:" + lessonInfoDto.teacher_id);
        String res ="";
        String token = lessonInfoDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                res = ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
                log.info("新增单节排课 返回1:" + res);
            }else{
                lessonInfoDto.create_user=chackToken.get("user").toString();
            }
        }else{
            res =  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            log.info("新增单节排课 返回2:" + res);

        }
        res =  courseLessonService.getLessonInfoDetail(lessonInfoDto);
        log.info("新增单节排课 返回3:" + res);
        return res;
    }

    /**
     * 复制保存备课并排课
     * @param copyLessonChapterDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/copyLessonChapter", method = RequestMethod.POST)
    @ResponseBody
    public String copyLessonChapter(@RequestBody CopyLessonChapterDto copyLessonChapterDto){
        log.info("复制保存备课并排课 teacher_id:" + copyLessonChapterDto.teacher_id);
        String res ="";
        String token = copyLessonChapterDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                res =  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
                log.info("复制保存备课并排课返回1:" + res);
                return  res;
            }else{
                copyLessonChapterDto.create_user=chackToken.get("user").toString();
            }
        }else{
            res =  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            log.info("复制保存备课并排课返回2:" + res);
           return res;
        }
        res = lessonChapterService.copyLessonChapter(copyLessonChapterDto);
        log.info("复制保存备课并排课返回3:" + res);
        return res;
    }

    /**
     * 根据班级id获取已被排课的科目
     * @param class_id
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getByClassID", method = RequestMethod.GET)
    @ResponseBody
    public String lessonGetByClassID(String class_id){
        String res ="";
        log.info("根据班级id获取已排课科目 class_id:" + class_id);
        res = lessonChapterService.lessonGetByClassID(class_id);
        log.info("根据班级id获取已排课科目返回:" + res);
        return res;
    }

    /**
     * 当前课程学生签到比率
     * @param lesson_id
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getCheckLessonRate", method = RequestMethod.GET)
    @ResponseBody
    public String checkLessonRate(String lesson_id){
        String res="";
        log.info("统计当前课程学生签到比率"+ lesson_id);
        res = courseLessonService.getCheckLesson(lesson_id);
        log.info("统计当前课程学生签到比率输出："+ res);
        return res;
    }

    /**
     *  统计当前科目和当前班级下学生
     *
     * @param courseLessonDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getStudentLessonRate", method = RequestMethod.POST)
    @ResponseBody
    public String getStudentLessonRate(@RequestBody CourseLessonDto courseLessonDto){
        String res="";
        log.info("统计当前课程学生签到比率"+ courseLessonDto.teacher_id);
        res = courseLessonService.getStudentLessonRate(courseLessonDto);
        log.info("统计当前课程学生签到比率输出："+ res);
        return res;
    }

    /**
     * 获取当前班级在当前科目下的备课情况
     */
    @RequestMapping(value = "/edu/k12/lesson/getCourseLesson", method = RequestMethod.POST)
    @ResponseBody
    public String getCourseLesson(@RequestBody CourseLessonDto courseLessonDto){
        String res="";
        log.info("获取当前班级在当前科目下的备课情况"+ courseLessonDto.class_id+",course"+courseLessonDto.course_id);
        String token = courseLessonDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        res = courseLessonService.getCourseLesson(courseLessonDto);
        log.info("获取当前班级在当前科目下的备课情况："+ res);
        return res;
    }
}

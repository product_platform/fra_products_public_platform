package com.unidt.api;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.services.edu.courselesson.CourseLessonService;
import com.unidt.services.edu.tools.SSOService;
import com.unidt.mybatis.dto.StudentInfoDto;
import com.unidt.mybatis.dto.TeacherClassSearchDto;
import com.unidt.services.edu.Student.StudentInfomation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@EnableAutoConfiguration
@ComponentScan
public class StudentInfoController {
    @Autowired
    StudentInfomation studentInfomation;
    @Autowired
    SSOService SSOService;

    @Autowired
    CourseLessonService courseLessonService;

    // 日志
    public static Logger log  = LoggerFactory.getLogger(StudentInfoController.class);

    /**
     * 根据ID查询学生详情
     * @param studentInfoDto
     * @return
     */
    @RequestMapping(value="/student/info", method = RequestMethod.POST)
    @ResponseBody
    public String studentInfomation(@RequestBody StudentInfoDto studentInfoDto){
        log.info("根据ID查询学生详情"+studentInfoDto.student_id);
        String token = studentInfoDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                studentInfoDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return studentInfomation.selectStudentInfo(studentInfoDto);
    }

    /**
     * 根据教师班级获取学生详情
     * @param teacherClassSearchDto
     * @return
     */
    @RequestMapping(value="/teacher/class/search", method = RequestMethod.POST)
    @ResponseBody
    public String teacherClassSearchStudent(@RequestBody TeacherClassSearchDto teacherClassSearchDto){
        log.info("根据教师班级获取学生详情"+"教师ID："+ teacherClassSearchDto.teacher_id);
        String token = teacherClassSearchDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                teacherClassSearchDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return studentInfomation.teacherClassSearchStudent(teacherClassSearchDto);
    }
}

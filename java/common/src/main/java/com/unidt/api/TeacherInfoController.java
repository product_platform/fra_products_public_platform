package com.unidt.api;

import com.unidt.services.edu.schedual.teacher.TeacherInfoService;
import com.unidt.services.edu.tools.SSOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@ComponentScan
public class TeacherInfoController {
    public static Logger log  = LoggerFactory.getLogger(TeacherInfoController.class);
    @Autowired
    TeacherInfoService infoService;
    /**
     * 根据ID获取教师基本信息
     * @param id
     * @return
     */
    @RequestMapping(value = "/edu/k12/teacher/info/get", method = RequestMethod.GET)
    @ResponseBody
    public String getTeacherInfo(String id) {
        log.info("根据ID获取教师基本信息" + id);
        return infoService.getTeacherInfo(id);
    }

    /**
     * 根据老师ID 获取教师名下班级列表和科目列表
     * @param id
     * @return
     */
    @RequestMapping(value = "/edu/k12/teacher/cc/get", method = RequestMethod.GET)
    @ResponseBody
    public String getTeacherCourseClass(String id) {
        log.info("根据老师ID 获取教师名下班级列表和科目列表"+ id);
        return infoService.getCourseClassInfo(id);
    }

    /**
     * 根据教师id获取班级列表信息
     * @param id
     * @return
     */
    @RequestMapping(value = "/edu/k12/teacher/getTeacherClassById", method = RequestMethod.GET)
    @ResponseBody
    public String getTeacherClassById(String id) {
        log.info("根据教师id获取班级列表信息"+ id);
        return infoService.getTeacherClassById(id);
    }


    /**
     * 根据老师id获取科目列表信息
     * @param id
     * @return
     */
    @RequestMapping(value = "/edu/k12/teacher/getTeacherCourseById", method = RequestMethod.GET)
    @ResponseBody
    public String getTeacherCourseById(String id) {
        String res ="";
        log.info("==根据老师id获取科目列表信息开始"+ id);
        res = infoService.getTeacherCourseById(id);
        log.info("根据老师id获取科目列表信息输出:"+ res);
        return res;
    }

}

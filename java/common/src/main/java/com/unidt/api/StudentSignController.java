package com.unidt.api;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.dto.StudentSignDto;
import com.unidt.services.edu.sign.StudentSignService;
import com.unidt.services.edu.tools.SSOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@EnableAutoConfiguration
@ComponentScan
public class StudentSignController {
    private static Logger log = LoggerFactory.getLogger(StudentSignController.class);
    @Autowired
    StudentSignService studentSignService;

    @Autowired
    SSOService SSOService;

    /**
     * 学生签到
     * @param studentSignDto
     * @return
     */
    @RequestMapping(value="/student/sign", method = RequestMethod.POST)
    @ResponseBody
    public String studentsign(@RequestBody StudentSignDto studentSignDto){
        log.info("学生签到"+"学生ID："+ studentSignDto.student_id + "课程ID："+ studentSignDto.lesson_id);
        String token = studentSignDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                studentSignDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return studentSignService.studentSign(studentSignDto);
    }
}

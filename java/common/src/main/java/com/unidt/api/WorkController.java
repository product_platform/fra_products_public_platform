package com.unidt.api;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.dto.HomeWorkContentDto;
import com.unidt.mybatis.dto.PublishWorkDto;
import com.unidt.mybatis.dto.ClassHomeWorkDto;
import com.unidt.services.edu.homework.HomeWorkService;
import com.unidt.services.edu.tools.SSOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static com.unidt.helper.common.Constants.API_TOKEN_DEADLINE;

@RestController
@EnableAutoConfiguration
@ComponentScan
public class WorkController {
    private static Logger log = LoggerFactory.getLogger(WorkController.class);

    @Autowired
    HomeWorkService homeWorkService;
    @Autowired
    SSOService SSOService;

    /**
     * 教师发布作业
     * @param publishWorkDto
     * @return
     */
    @RequestMapping(value="/teacher/publish", method = RequestMethod.POST)
    @ResponseBody
    public String workPublish(@RequestBody PublishWorkDto publishWorkDto){
        log.info("教师发布作业"+" 章节ID：" + publishWorkDto.chapter_id);
        String ret = SSOService.tokenCheck(publishWorkDto.token);
        if (!ret.equals("True")) return ret;
        return homeWorkService.publishWork(publishWorkDto);
    }

    /**
     * 查询章节作业
     * @param publishWorkDto
     * @return
     */
    @RequestMapping(value="/chapter/homework", method = RequestMethod.POST)
    @ResponseBody
    public String ChapterHomework(@RequestBody PublishWorkDto publishWorkDto){
        log.info("查询章节作业"+" 章节ID：" + publishWorkDto.chapter_id);
        String ret = SSOService.tokenCheck(publishWorkDto.token);
        if (!ret.equals("True")) return ret;
        return homeWorkService.chapterHomework(publishWorkDto);
    }

    /**
     * 查询当天课程
     * @param classHomeWorkDto
     * @return
     */
    @RequestMapping(value="/class/homework/getLessonsByDate", method = RequestMethod.POST)
    @ResponseBody
    public String classHomeworkByDate(@RequestBody ClassHomeWorkDto classHomeWorkDto ){
        log.info("查询当天课程"+" 日期：" + classHomeWorkDto.date + " 教师ID："+ classHomeWorkDto.teacher_id);
        String token = classHomeWorkDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                classHomeWorkDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return homeWorkService.classHomeworkByDate(classHomeWorkDto);
    }

    /**
     *   根据学生id/作业id/课程章节id/获取作业
     * @param homeWorkContentDto
     * @return
     */
    @RequestMapping(value="/class/homework/content", method = RequestMethod.POST)
    @ResponseBody
    public String homeworkContent(@RequestBody HomeWorkContentDto homeWorkContentDto){
        log.info("查询作业内容详情" + "作业ID：" + homeWorkContentDto.work_id);
        String token = homeWorkContentDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                homeWorkContentDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return homeWorkService.homeworkContent(homeWorkContentDto);
    }
}

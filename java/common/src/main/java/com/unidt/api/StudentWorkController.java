package com.unidt.api;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.dto.LessonChapterIDDto;
import com.unidt.mybatis.dto.LessonIDDto;
import com.unidt.services.edu.homework.StudentLessonWorkService;
import com.unidt.services.edu.tools.SSOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.unidt.helper.common.Constants.API_CODE_FORBIDDEN;

@RestController
@EnableAutoConfiguration
@ComponentScan
public class StudentWorkController {
    private static Logger log = LoggerFactory.getLogger(StudentWorkController.class);
    @Autowired
    StudentLessonWorkService lessonWorkService;
    @Autowired
    SSOService SSOService;


    /**
     * 根据课程获取学生作业完成情况
     * @param lessonid
     * @return
     */
    @RequestMapping(value = "/edu/k12/student/getStudentWorkByLesson", method = RequestMethod.GET)
    public String getStudentWorkByLesson(String lessonid) {
        log.info("根据课程获取学生作业完成情况"+ lessonid);
        return lessonWorkService.getStudentWorkByLesson(lessonid);
    }

    /**
     * 根据课程获取学生作业完成情况
     * @param lessonChapterIDDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getStudentWorksById", method = RequestMethod.POST)
    public String getStudentWorkByLesson(@RequestBody LessonChapterIDDto lessonChapterIDDto) {
        log.info("根据课程获取学生作业完成情况"+ lessonChapterIDDto);
        String lesson_chapter_id =lessonChapterIDDto.lesson_chapter_id;
        String work_id=lessonChapterIDDto.work_id;
        String token = lessonChapterIDDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(API_CODE_FORBIDDEN, "token验证失败！").toJson();
            }else{
                lessonChapterIDDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(API_CODE_FORBIDDEN, "token验证失败！").toJson();
        }
        return lessonWorkService.getLessonWorkByworkId(lesson_chapter_id,work_id);
    }

    /**
     * 教师端: 学生作业提交率
     * @param lessonIDDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getUpWorkRate", method = RequestMethod.POST)
    public String getStudentWorkPercentByLesson(@RequestBody LessonIDDto lessonIDDto) {
        log.info("获取学生作业提交率"+" 课程ID：" + lessonIDDto.lesson_id);

        String token = lessonIDDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                lessonIDDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return lessonWorkService.getStudentWorkPercentByLesson(lessonIDDto);
    }

    /**
     * 根据课程ID学生ID获取当前课程作业提交情况
     * @param lessonIDDto
     * @return
     */
    @RequestMapping(value = "/edu/k12/lesson/getWorkByStudent", method = RequestMethod.POST)
    public String getStudentWorkByLesson(@RequestBody LessonIDDto lessonIDDto) {
        log.info("根据学生ID和课程ID获取学生作业"+" 学生ID：" + lessonIDDto.student_id);
        String token = lessonIDDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                lessonIDDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return lessonWorkService.getStudentWorkByLessonStudent(lessonIDDto);
    }
}

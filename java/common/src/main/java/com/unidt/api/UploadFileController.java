package com.unidt.api;

import com.unidt.helper.common.Constants;
import com.unidt.helper.common.ReturnResult;
import com.unidt.mybatis.dto.UploadDto;
import com.unidt.mybatis.dto.UploadStudentWorkDto;
import com.unidt.mybatis.mapper.SSOMapper;
import com.unidt.services.edu.tools.SSOService;
import com.unidt.services.edu.upload.UploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static com.unidt.helper.common.Constants.API_CODE_FORBIDDEN;

@RestController
@EnableAutoConfiguration
@ComponentScan
public class UploadFileController {
    private static Logger log = LoggerFactory.getLogger(UploadFileController.class);
    @Autowired
    UploadService uploadService;
    @Autowired
    SSOMapper ssoMapper;
    @Autowired
    SSOService SSOService;


    /**
     * 上传教师资源
     * @param uploadDto
     * @return
     */
    @RequestMapping(value="/upload/save", method = RequestMethod.POST)
    @ResponseBody
    public String saveUploadFile(@RequestBody UploadDto uploadDto){
        log.info("上传教师资源"+ "教师ID："+ uploadDto.teacher_id);
        String ret = SSOService.tokenCheck(uploadDto.token);
        if (!ret.equals("True")) return ret;
        return uploadService.saveFile(uploadDto);
    }

    /**
     * 更新教师资源
     * @param uploadDto
     * @return
     */
    @RequestMapping(value="/upload/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateUploadFile(@RequestBody UploadDto uploadDto){
        log.info("更新教师资源"+ "教师ID："+ uploadDto.teacher_id);
        String token = uploadDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                uploadDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return uploadService.updateFile(uploadDto);
    }

    /**
     * 上传学生作业
     * @param uploadStudentWorkDto
     * @return
     */
    @RequestMapping(value="/upload/studentwork", method = RequestMethod.POST)
    @ResponseBody
    public String uploadStudentWork(@RequestBody UploadStudentWorkDto uploadStudentWorkDto){
        log.info("上传学生作业");
        String token = uploadStudentWorkDto.token;
        if(!StringUtils.isEmpty(token)){
            Map chackToken = SSOService.getUserByToken(token);
            if(!(Boolean)chackToken.get("token")){
                return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
            }else{
                uploadStudentWorkDto.create_user=chackToken.get("user").toString();
            }
        }else{
            return  ReturnResult.createResult(Constants.API_TOKEN_DEADLINE, "token验证失败！").toJson();
        }
        return uploadService.uploadStudentWork(uploadStudentWorkDto);
    }






}


